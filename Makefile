
build_all: build_facefilm build_web build_worker
pull_all: pull_facefilm pull_web pull_worker

# Facefilm
build_facefilm:
	cd src; \
		docker build -t vierja/facefilm:latest .
pull_facefilm:
	docker pull vierja/facefilm:latest

# Web
build_web:
	cd web; \
		docker build -t vierja/facefilm:web .
pull_web:
	docker pull vierja/facefilm:web

# Worker
build_worker:
	cd worker; \
		docker build -t vierja/facefilm:worker .
pull_worker:
	docker pull vierja/facefilm:worker
