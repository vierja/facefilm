### Introducción


La detección de caras es un problema complejo, consiste en determinar la posición y tamaño entre otras propiedades, de las caras en una determina imagen. Existen muchas formas de localizar caras pero ninguna se compara con la habilidad humana.

Al tratarse de imágenes, la resolución del problema de detección de caras tiende ser un proceso tanto CPU-bound como I/O-bound.

Se busca, a partir de las fotos de Facebook de usuario, realizar la generación de un video donde la cara del usuario en cuestioón se mantiene siempre fija en el centro del cuadro, cambiando rápidamente en orden cronológico las fotos.

Esto está inspirado en una funcionalidad del software [Picasa](picasa.google.com) llamada [Face Movie](https://support.google.com/picasa/answer/187259?hl=en), donde se realiza lo mismo usando las fotos que se tienen etiquedatas internamente.

Se puede ver un ejemplo de un Face Movie generado con Picasa en el [siguiente enlace](http://www.youtube.com/watch?v=fLQtssJDMMc).

Se intenta reproducir la funcionalidad, y probar la viabilidad de ofrecer la generación del video abiertamente a usuarios en tiempo real usando High Performance Computing.


### Presentación del problema

##### Detección de caras

Para el problema de detección de caras se busca
 

### Estrategia de Resolución

##### Procesamiento de imágenes

Para el procesamiento de imágenes se decide usar la biblioteca de código abierto de procesamiento de visión OpenCV. Originalmente creada por Intel, hoy en día es referente en el área de computer vision, proveyendo distintos algoritmos de manejo de imágenes, detección y clasificación de objetos y aprendizaje automático entre otros.

Las imágenes se deben de trasladar, rotar y escalar de manera que la posición de la cara del usuario quede siempre en el centro de la imagen. Para esto se utilizan transformaciones afines, calculando la matriz de la transformación utilizando puntos de referencia y aplicándola sobre la imagen en cuestión.

También para minimizar el impacto en el cambio entre imagen e imagen se utilizan primitivas de OpenCV que permiten la superposición variando el canal alpha de estas, para crear un conjunto de imágenes de transición fluida.

##### Detección de caras

OpenCV provee diferentes algoritmos para la detección de objetos, entre ellos el clasificador binario usando características pseudo-Haar. 

Las clasificación usando caráterísticas pseudo-Haar, o Haar-like features clasification, es una forma efectiva de detección de objetos usando aprendizaje de imágenes positivas (en nuestro caso, caras), e imágenes negativas (resto). Se utilizan 3 tipos de carácterísticas pseudo-Haar para entrenar el clasificador, cada una de estas obtiene un valor único restándole a la suma de los píxeles en la zona negra de la suma de los píxeles en la zona blanca.

![image](http://i.imgur.com/aGsgwuJ.jpg)

De esta manera, usando imágenes de control tanto positivas como negativas se crea un clasificador que consiste en distintas características pseudo-Haar que tienen las caras.

La implementación de OpenCV, basada en el framework Viola-Jones, utilizando [Adapting Boosting](http://en.wikipedia.org/wiki/AdaBoost) y clasificación en cascada, la cual descarta rápidamente secciones de una imagen, permite una clasificación potente y rápida.

OpenCV, además de proveer el algoritmo, provee los archivos de entrenamientos ya realizados tanto de caras, como de distintas características faciales y corporales que pueden ser utilizadas para obtener un producto final más refinado.

##### Generación de video

La generación de video se realiza usando la herramienta [ffmpeg](http://www.ffmpeg.org/). Esta herramienta multipropósito permite la generación/edición, conversión y manipulación general de video de muchos formatos. Para la generación del video se corre el ffmpeg con _popen_, definiendo de esta manera el pipe al cual se escriben las imágenes. La función _popen_ crea un nuevo proceso creando un nuevo pipe, haciendo fork e invocando el programa con la shell. Se puede abrir un proceso sólo de escritura o lectura, para nuetro caso se crea uno únicamente de escritura.

Cuando se termina con los cuadros del video, simplemente se cierra el _pipe_ usando _pclose_ y ffmpeg termina de escribir en el video.

### Uso de High Performance Computing

Dado la gran cantidad de tiempo que cuesta detectar caras, modificar imagenes, y generar video, es primordial poder distribuir las tareas para poder hacer este trabajo en tiempo real.

Se diseñaron e implementaron dos arquitecturas distintas. Una separa el problema en N, y genera N videos de forma independiente para luego lugar los N videos en uno.

La segunda, más compleja, consiste en un coordinador que de forma ordenada le da imagenes a los esclavos para que encuentren la cara, la modifiquen centrando la imagen, y envien el resultado al finalizar, y un generador de video dedicado que recibe del master las imagenes en orden que tiene que poner en el video.

##### Separar el problema en N

Este diseño básico, permite la total paralelización del problema, pero se pierde en parte uno de los objetivos del trabajo, que es permitir la visualizacion en tiempo real de dicho video a medida que se va generando.

Consiste que el master, nodo principal, separa la lista de imagenes a procesar de forma equitativa y envia cada parte a cada uno de los esclavos que se tenga a disposicion. Estos esclavos trabajan de forma totalmente independiente generando para cada una de las sublistas de imagenes que recibieron el video resultado.

Al finalizar la generacion de cada uno de estos videos, se guarda el archivo en un directorio compartido, y se envía la ubicacion de este al master. El master recibe uno a uno las ubicaciones de los videos resultado y usando ffmpeg junta los videos en el orden correspondiente generando el video final.

A primera vista sin pruebas experimentales para analizar se pueden notar varias ventajas y desventajas. Como ventaja, la simplicidad de la arquitectura, total paralelizacion de la generacion de video, y potencialmente puede escala facilmente a cualquier cantidad de nodos. Las desventajas incluyen no poder generar el video en orden, y al momento de juntar los videos resultados, sin realizar un algoritmo complejo, se pierde el efecto transición entre la ùltima imagen de un video y la primera imagen del siguiente, también el master tiene muy poco trabajo y responsabilidades desperdiciando recursos.	

##### Detección distribuida, generación única

Esta arquitectura, más compleja que la anterior permite aprovechar el trabajo de todos los nodos y al mismo tiempo generar un único video en órden. De alcanzarse la meta de generación en tiempo real esta arquitectura podría potencialmente generar el video y hacer streaming directamente al usuario para la visualizacion directa del video.

Consiste en un nodo master, el coordinador, un nodo renderer que se dedica exclusivamente a generar el video, y N nodos esclavos que reciben imagenes, detectan caras, modifican imagenes y retornan el resultado al master. 

![image](http://i.imgur.com/yg8Okk2.png)

Como MPI no está pensado para envió de datos grandes, como pueden ser las imagenes, para la transferencia imagenes entre nodos se guardan los datos en el directorio compartido, y se pasa como mensaje la ubicacion de este.

El master recibe la lista inicial de imagenes y es la que mantiene la informacion del status de cada imagen. Cada imagen puede estar pendiente, en proceso, sin cara encontrada, o con cara encontrada. Consiste en un loop infinito que reciben el mensaje de acciones de los nodos trabajados.

Si el master recibe un mensaje de busqueda de trabajo, busca la siguiente imagen pendiente, la marca como en proceso y se la envía al nodo.

Si no es en busca de trabajo, el nodo puede enviar mensaje de resultado, tanto cara encontrada como cara no encontrada. En el caso de cara no encontrada se marca la imagen en cuestion como no encontrada y se continua con el loop, en caso de cara encontrada se marca como encontrada y se manda la referencia de la imagen resultado al renderer.

El renderer trabaja con un buffer, es el único al que se le envían los mensajes de forma asincrónica. Al igual que todos trabaja con un loop recibiendo imagenes, generando la transicion con la imagen anterior y enviando los datos de los distintos cuadros al _ffmpeg_.

##### Logger

Para poder loguear los eventos en el orden correcto en el tiempo correcto, se usa un nodo especial _logger_ el cual recibe mensajes de todos los nodos y los imprime. De esta manera al ver los resultados se pueden ver en el orden y tiempo correcto.

Para las pruebas en las que se toma en cuenta el tiempo total en finalizar no se usa el _logger_ por un tema de performance de envíos de mensajes bloqueantes innecesarios.


### Pseudocódigo

### Comunicación entre procesos
### Análisis experimental
