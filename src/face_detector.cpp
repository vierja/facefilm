#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>

using namespace cv;
using namespace std;

#include "face_detector.h"

#define PI 3.141592

bool FaceDetector::getFace(Mat img, Point2f tag_f, int num_tags, Rect& face)
{
    /*
        Returns the face, pair of eyes, mouth most likely to the correct one
        based on the image and the tag.
    */

    if (img.empty())
    {
        cout << "Error: Image cannot be loaded." << endl;
        return false;
    }

    Size size = img.size();

    int tag_x = (int)(tag_f.x * size.width) / 100.0;
    int tag_y = (int)(tag_f.y * size.height) / 100.0;

    Point tag = Point(tag_x, tag_y);

    double search_ratio = min(max(2.0f / (double) num_tags, 0.25), 1.0);

    int new_width = size.width * search_ratio;
    int new_height = size.height * search_ratio;

    int reshape_init_x = max(tag_x - new_width / 2, 0);
    int reshape_init_y = max(tag_y - new_height / 2, 0);

    int reshape_end_x = min(reshape_init_x + new_width, size.width - 1);
    int reshape_end_y = min(reshape_init_y + new_height, size.height - 1);

    Range colRange = Range(reshape_init_x, reshape_end_x);
    Range rowRange = Range(reshape_init_y, reshape_end_y);

    Mat reshapeImg = Mat(img, rowRange, colRange);

    Point reshapePoint = Point(reshape_init_x, reshape_init_y);

    tag -= reshapePoint;

    size = reshapeImg.size();

    Mat img_grey;
    cvtColor(reshapeImg, img_grey, COLOR_BGR2GRAY);

    equalizeHist(img_grey, img_grey);

    vector<Rect> faces;
    int res = findFaces(img_grey, faces);
    if (res == -1)
    {
        return false;
    } else if (res == 0)
    {
        return false;
    }

    res = bestFace(faces, tag, size, face);
    if (res == -1)
    {
        return false;
    }

    face += reshapePoint;

    return true;
}

void FaceDetector::transformFeatures(Mat transformation, vector<Point2f*> features)
{
    int count = 0;

    int vectorSize = features.size();

    while (count < vectorSize)
    {
        Mat pointMat = Mat::zeros(3, 1, transformation.type());
        pointMat.at<double>(0,0) = features[count]->x;
        pointMat.at<double>(1,0) = features[count]->y;
        pointMat.at<double>(2,0) = 1;

        Mat dstPointMat = transformation * pointMat;

        //auto dstPoint = new Point2f(dstPointMat.at<double>(0,0), dstPointMat.at<double>(0,1));

        //cout << "dstPoint: " << dstPoint << " ,";
        features[count]->x = dstPointMat.at<double>(0,0);
        features[count]->y = dstPointMat.at<double>(0,1);

        count ++;
    }
}

void FaceDetector::transformRect(Mat transformation, Rect &rect, vector<Point2f*> &rotated_rect)
{

    rotated_rect.push_back(new Point2f(rect.x, rect.y));
    rotated_rect.push_back(new Point2f(rect.x + rect.width, rect.y));
    rotated_rect.push_back(new Point2f(rect.x + rect.width, rect.y + rect.height));
    rotated_rect.push_back(new Point2f(rect.x, rect.y + rect.height));

    this->transformFeatures(transformation, rotated_rect);

}


int FaceDetector::centerFace(Mat img, Rect &face, Mat &res, vector<Point2f*> features, vector<Point2f*> &rotatedFace)
{
    /*
        Using the 8 feature points we rotate the face to get the eyes aligned horizontally
        and we use the diagonal of the face rect as as scale.

        Movement consist on three affine transformations combined.

        -> Rotation to align eyes
        -> Scale to result in similar face size
        -> Translation to fit center of face center of image

        Because of the way the transformations work, we split initially create a basic translation
        and then ajust it because of the rotation center point.
    */

    if (features.size() != 8)
    {
        return 0;
    }

    // The final image size.
    int width = 1280;
    int height = 720;

    res = Mat::zeros(height, width, img.type());

    /*
        We use the eye points as the center of each pair of points.
    */
    // 2  ojoIzquierdo  6
    auto leftEye = Point2f((features[2]->x + features[6]->x) / 2, (features[2]->y + features[6]->y) / 2);
    //5  ojoDerecho  1
    auto rightEye = Point2f((features[1]->x + features[5]->x) / 2, (features[1]->y + features[5]->y) / 2);

    // We use the face diagonal size for scaling reference.
    double scale = 100/distanceBetweenPoints(Point2f(face.x, face.y), Point2f(face.x + face.width, face.y + face.height));

    // Rotation_mat = 2 x 3
    /*
        ⍺    β    (1 - ⍺).center.x - β.center.y
       -β    ⍺    β.center.x + (1 - ⍺).center.y
    */ 
    Mat rotation_mat = this->getRotationMatrix(rightEye, leftEye, scale);

    // Add third row (0 0 1) to be able to multiply it with another 3x3.
    Mat lastRow = Mat::zeros(1, 3, rotation_mat.type());  // 3 cols
    lastRow.at<double>(0, 2) = 1;
    rotation_mat.push_back(lastRow);

    // Translation matrix. Calculate movement to get the center of the face to the center of the image.
    int x_diff = width/2 - features[0]->x;
    int y_diff = height/2 - features[0]->y;
    Mat basic_translation_mat = Mat::eye(3, 3, rotation_mat.type());
    basic_translation_mat.at<double>(0,2) = x_diff;
    basic_translation_mat.at<double>(1,2) = y_diff;

    /*
        Compine transformations multiplying them.
    */
    Mat translateAndRotate = rotation_mat * basic_translation_mat;

    /*
        Translate and rotate face center and apply inverse translation difference to transalteAndRotate transformation.
    */
    Mat faceCenterMat = Mat::zeros(3, 1, translateAndRotate.type());
    faceCenterMat.at<double>(0,0) = features[0]->x;
    faceCenterMat.at<double>(1,0) = features[0]->y;
    faceCenterMat.at<double>(2,0) = 1;

    Mat dstFaceCenterMat = translateAndRotate * faceCenterMat;

    Mat inverseTraslationTranformation = Mat::eye(3, 3, translateAndRotate.type());
    inverseTraslationTranformation.at<double>(0,2) = width/2 - dstFaceCenterMat.at<double>(0,0);
    inverseTraslationTranformation.at<double>(1,2) = height/2 - dstFaceCenterMat.at<double>(1,0);

    translateAndRotate = inverseTraslationTranformation * translateAndRotate;

    // Remove last row to make it a 2 * 3 matrix suitable for warpAffine function.
    translateAndRotate = translateAndRotate.rowRange(0, translateAndRotate.rows - 1);

    warpAffine(img, res, translateAndRotate, res.size(), INTER_LINEAR, BORDER_CONSTANT, Scalar(0,0,0,0));

    // We also need to modify the feature points in order to match the final image.
    this->transformFeatures(translateAndRotate, features);
    cout << "face:" << face << "\n";
    this->transformRect(translateAndRotate, face, rotatedFace);
    cout << "rotatedFace:" << face.size() << "\n";

    return 1;
}

int FaceDetector::findFaces(Mat img_grey, vector<Rect> &faces)
{
    /*
        Returns all the faces found in the image found with the different cascades defined.
    */

    /*
        CascadeClassifier::detectMultiScale Parameters:
            - cascade – Haar classifier cascade (OpenCV 1.x API only). It can be loaded from XML or YAML file using Load(). When the cascade is not needed anymore, release it using cvReleaseHaarClassifierCascade(&cascade).
            - image – Matrix of the type CV_8U containing an image where objects are detected.
            - objects – Vector of rectangles where each rectangle contains the detected object.
            - scaleFactor – Parameter specifying how much the image size is reduced at each image scale.
            - minNeighbors – Parameter specifying how many neighbors each candidate rectangle should have to retain it.
            - flags – Parameter with the same meaning for an old cascade as in the function cvHaarDetectObjects. It is not used for a new cascade.
            - minSize – Minimum possible object size. Objects smaller than that are ignored.
            - maxSize – Maximum possible object size. Objects larger than that are ignored.
    */

    int num_faces = 0;

    for (vector<CascadeClassifier>::iterator it = face_cascades.begin() ; it != face_cascades.end(); ++it)
    {
        CascadeClassifier face_cascade = (CascadeClassifier) *it;
        vector<Rect> cascade_faces;
        try
        {
            face_cascade.detectMultiScale(img_grey, cascade_faces, 1.2, 4, 0|CV_HAAR_SCALE_IMAGE, Size(50, 50));

            for (vector<Rect>::iterator face = cascade_faces.begin() ; face != cascade_faces.end(); ++face)
            {
                faces.push_back((Rect) *face);
                num_faces++;
            }
        }
        catch( cv::Exception& e )
        {
            const char* err_msg = e.what();
            cout << "exception caught: " << err_msg << endl;
        }
    }

    return num_faces;
}

int FaceDetector::bestFace(vector<Rect> faces, Point tag, Size size, Rect &face)
{
    /*
        Chooses the best face of a list of faces based on the tag.
    */

    sortByDistance(faces, tag);

    vector<Rect>::iterator it = faces.begin();

    // Itero sobre la lista ordenada por distancia de caras.
    while (it != faces.end())
    {
        // Si la distancia es mayor a un valor determinado entonces dejo de iterar.
        if (!validFace((Rect) *it, tag))
        {
            break;
        }

        // Si no esta definido face (primera iteracion) defino la cara mas cercana.
        if (face.area() == 0)
        {
            face = (Rect) *it;
        } else
        {
            // Si no, busco la interseccion entre la nueva y la anterior.
            Rect intersection;
            intersect(face, (Rect) *it, intersection);
            // Si comparten mas del 75% del area de la mas chica entonces defino
            // la nueva cara como la interseccion entre estas.
            double minShared = minSharedArea(face, (Rect) *it, intersection);

            if (minShared > 0.75)
            {
                face = intersection;
            }
        }

        it++;
    }

    if (face.area() == 0)
    {
        return -1;
    }

    return 1;
}

int FaceDetector::findEyesMouth(Mat img_grey, Rect face, Point reshapePoint, vector<Point> &eyes, vector<Rect> &mouth)
{
    /*
        Finds eyes and mouth inside the face in the image.
    */
    Mat face_roi = img_grey(face);

    vector<Rect> eyes_vector;

    int num_eyes = 0;
    for (vector<CascadeClassifier>::iterator it = eyes_cascades.begin() ; it != eyes_cascades.end(); ++it)
    {
        CascadeClassifier eyes_cascade = (CascadeClassifier) *it;
        vector<Rect> cascade_eyes;
        try
        {
            eyes_cascade.detectMultiScale(face_roi, cascade_eyes, 1.1, 4, 0|CV_HAAR_SCALE_IMAGE, Size(0, 0));

            for (vector<Rect>::iterator eye = cascade_eyes.begin(); eye != cascade_eyes.end(); ++eye)
            {
                Rect relocate = ((Rect) *eye);
                eyes_vector.push_back(relocate);
                num_eyes++;
            }
        }
        catch( cv::Exception& e )
        {
            const char* err_msg = e.what();
            cout << "exception caught: " << err_msg << endl;
        }
    }

    cout << "Found " << num_eyes << " eyes.\n";

    int num_mouth = 0;
    for (vector<CascadeClassifier>::iterator it = mouth_cascades.begin() ; it != mouth_cascades.end(); ++it)
    {
        CascadeClassifier mouth_cascade = (CascadeClassifier) *it;
        vector<Rect> cascade_mouths;
        try
        {
            mouth_cascade.detectMultiScale(face_roi, cascade_mouths, 1.1, 4, 0|CV_HAAR_SCALE_IMAGE, Size(0, 0));

            for (vector<Rect>::iterator mouth_rect = cascade_mouths.begin(); mouth_rect != cascade_mouths.end(); ++mouth_rect)
            {
                Rect relocate = ((Rect) *mouth_rect) + Point(face.x, face.y)  + reshapePoint;
                mouth.push_back(relocate);
                num_mouth++;
            }
        }
        catch( cv::Exception& e )
        {
            const char* err_msg = e.what();
            cout << "exception caught: " << err_msg << endl;
        }
    }

    this->selectTwoEyes(face, reshapePoint, eyes_vector, eyes);

    cout << " two_eyes:" << eyes.size() << ":two_eyes\n";

    return 1;
}

int FaceDetector::selectTwoEyes(Rect face, Point reshapePoint, vector<Rect> eyes_vector, vector<Point> &eyes)
{
    if (eyes_vector.size() == 0)
    {
        return 0;
    }

    // Filtro mitad para arriba y los separo entre ojos de izquierda y derecha.
    vector<Rect> right_eyes; // Mi derecha
    vector<Rect> left_eyes; // Mi izquierda

    int half_height = face.height / 2;
    int half_width = face.width / 2;

    for (vector<Rect>::iterator it = eyes_vector.begin(); it != eyes_vector.end(); ++it)
    {
        Rect eye = (Rect) *it;

        // X, Y es desde la esquina superior izquierda
        if ((eye.y + (eye.height / 2)) < half_height)
        {
            if ((eye.x + (eye.width / 2)) > half_width)
            {
                // Mi derecha.
                right_eyes.push_back(eye);
            } else
            {
                left_eyes.push_back(eye);
            }
        }
    }

    if (right_eyes.size() == 0 || left_eyes.size() == 0)
    {
        return 0;
    }

    Point right_eye = this->findPseudoCentroid(right_eyes) + reshapePoint + Point(face.x, face.y);
    Point left_eye = this->findPseudoCentroid(left_eyes) + reshapePoint + Point(face.x, face.y);

    eyes.push_back(right_eye);
    eyes.push_back(left_eye);
    // eyes = [ right_eyes, left_eye];
    //           eyes[0]     eyes[1]
    return 1;
}

Point FaceDetector::findPseudoCentroid(vector<Rect> rects)
{
    /*
        Primero buscamos el centroid real.
        Luego ordenamos por distancia a este y sacamos los que estan muy lejos.
    */

    Point centroid = Point(0, 0);

    int num_rects = rects.size();

    for (vector<Rect>::iterator it = rects.begin(); it != rects.end(); ++it)
    {
        Point center = Point(((Rect) *it).x + (((Rect) *it).width / 2), ((Rect) *it).y + (((Rect) *it).height / 2));
        centroid += center;
    }

    centroid = Point(centroid.x / num_rects, centroid.y / num_rects);

    return centroid;
}

bool intersection(Point2f o1, Point2f p1, Point2f o2, Point2f p2, Point2f &r)
{
    Point2f x = o2 - o1;
    Point2f d1 = p1 - o1;
    Point2f d2 = p2 - o2;

    float cross = d1.x*d2.y - d1.y*d2.x;
    if (abs(cross) < /*EPS*/1e-8)
        return false;

    double t1 = (x.x * d2.y - x.y * d2.x)/cross;
    r = o1 + d1 * t1;
    return true;
}

float slop(Point2f o1, Point2f p1)
{
    auto x_diff = o1.x - p1.x;
    auto y_diff = o1.y - p1.y;

    if (x_diff == 0)
    {
        return 0;
    } else
    {
        return y_diff/x_diff;
    }
}

Mat FaceDetector::getRotationMatrix(Point2f rightEye, Point2f leftEye, double scale)
{
    /*
        Right eye: HIS right eye.
        Left eye: HIS right eye.

        Right eye      Left eye

                mouth

    */
    int videoHeight = 720;
    int videoWidth = 1280;

    /*

        Puntos de recta horizontal. Usamos videoHeight para que el punto de interseccion de la recta sea intuitivo.
        Si fuera potencialmente una recta por arriba de lo ojos complica un poco entenderlo.


    --------------------------------------------------------------------------
    |                                                                        |
    |                                                                        |
    |                                                                        |
    |               oR                                                       |
    |               .       oL`                                              |
    |              .                .                                        |
    |             .                      .                                   |
    |            .                           .                               |
    |          oL                                 .                          |
    |         .                                        .                     |
    |        .                                             .                 |
    |       .                                                   .            |
    |______x________________________________________________________x'_______|
    */

    Point2f horiz1 = Point2f(0, videoHeight);
    Point2f horiz2 = Point2f(100, videoHeight);

    Point2f intersection_point;

    bool res = intersection(horiz1, horiz2, rightEye, leftEye, intersection_point);
    if (!res)
    {
        cout << "No hay interseccion entre rectas de ojos\n";
        // Si no hay intersección, son paralelas o pseudo paralelas entoncs no se rota.

            cout << "Esto solo puede suceder si leftEye.y == rightEye.y : " << (leftEye.y == rightEye.y) << " (true si iguales)\n";

        // Solo se tiene que hacer la traslacion y la escala. TODO:
        return getRotationMatrix2D(leftEye, 0, scale);
    }

    cout << "Se encuentra la intersection entre (horizontal) [horiz1, horiz2] = [" << horiz1 << ", " << horiz2 << "] y ";
    cout << "(originales) [rightEye, leftEye] = [" << rightEye << ", " << leftEye << "] como el punto = " << intersection_point << "\n";

    cout << "The intersection point should have y value (" << intersection_point.y << ") == videoHeight (" << videoHeight << ") = " << (intersection_point.y == videoHeight) << "\n";

    /*
          leftEye
        |\
        | \
        |  \ hipotenusa
        |   \
        |____\ intersection point
leftEye.x, y=videoHeight
         adyancente

        Para obtener el angulo entre la hipotenusa y la adyacente, se hace acos(adyacente/hipotenusa)
    */

    Point2f adjPoint = Point2f(leftEye.x, videoHeight);

    double hypDistance = distanceBetweenPoints(leftEye, intersection_point);
    double adjDistance = distanceBetweenPoints(adjPoint, intersection_point);

    cout << "hypDistance: " << hypDistance << "\n";
    cout << "adjDistance: " << adjDistance << "\n";

    double angle = acos(adjDistance/hypDistance);

    cout << "Angle (as acos(adjDistance/hypDistance): " << angle << "\n";
    cout << "Angle  * 180 / PI = " << angle * 180 / PI << "\n";

    angle = angle * 180 / PI;

    /*
        Rotation angle in degrees. Positive values mean counter-clockwise rotation (the coordinate origin is assumed to be the top-left corner).
    */

    if (intersection_point.x < leftEye.x)
    {
        angle = -angle;
        cout << "El centro de la interseccion se encuentra a la derecha de los objetos. Se aplica la rotacion opuesta. Nuevo angulo " << angle << "\n";
    }

    return getRotationMatrix2D(leftEye, angle, scale);

}

int FaceDetector::loadCascade(vector<string> cascade_names, vector<CascadeClassifier>& cascades)
{
    int loaded_cascades = 0;
    for (vector<string>::iterator it = cascade_names.begin() ; it != cascade_names.end(); ++it)
    {
        CascadeClassifier cascade;
        if (!cascade.load(((string)*it).c_str()) ) {
            cout << "Error: Couldn't load cascade: " << *it << ".\n";
        } else {
            cascades.push_back(cascade);
            loaded_cascades++;
        }
    }

    cout << "Loaded a total of " << loaded_cascades << " cascades. " << cascades.size() << "\n";

    return loaded_cascades;
}

int FaceDetector::sortByDistance(vector<Rect> &faces, Point tag)
{

    sort(faces.begin(), faces.end(), Local(tag));

    return 1;
}

double FaceDetector::minSharedArea(Rect rect1, Rect rect2, Rect intersect)
{
    /*
        Returns the min shared area between two rect.
        The min shared area is the min between
            - intersection vs rect1
            - intersection vs rect2
    */

    int rect1Area = rect1.area();
    int rect2Area = rect2.area();
    int intersectArea = intersect.area();

    double rect1Shared = (double) intersectArea / (double) rect1Area;
    double rect2Shared = (double) intersectArea / (double) rect2Area;

    return min(rect1Shared, rect2Shared);
}

int FaceDetector::intersect(Rect rect1, Rect rect2, Rect &intersection)
{
    /*
        Returns the intersection between the rect1 and rect2.
    */

    intersection = rect1 & rect2;

    return 1;
}

bool FaceDetector::validFace(Rect face, Point tag)
{
    // Si el tag esta adentro de la cara entonces es valida.
    if (face.contains(tag))
    {
        return true;
    }
    // Por ahora retornamos false si no esta adentro.
    // TODO: Mejora este algoritmo.
    return false;
}

vector<Point2f*> FaceDetector::detectFeatures(Mat img, Rect face)
{
    /*
        El orden de los puntos es:


         5  ojoDerecho  1    2  ojoIzquierdo  6

                          0
                     face center
                          7
                        nariz



                     3  boca  4

    */
    cout << "Detecting features\n";
    int *bbox = (int*)malloc(4*sizeof(int));
    double *landmarks = (double*)malloc(2*this->model->data.options.M*sizeof(double));

    bbox[0] = face.x;
    bbox[1] = face.y;
    bbox[2] = face.x + face.width;
    bbox[3] = face.y + face.height;


    auto tmp = (IplImage)img;

    IplImage* input = cvCloneImage(&tmp);
    IplImage *frame_bw = cvCreateImage(cvSize(input->width, input->height), IPL_DEPTH_8U, 1);
    cvConvertImage(input, frame_bw);

    cout << "Finished with preinit data\n";

    flandmark_detect(frame_bw, bbox, this->model, landmarks);

    cout << "Detected\n";

    vector<Point2f*> points;

    for (int i = 0; i < 2*model->data.options.M; i += 2)
    {
        auto point = new Point2f(double(landmarks[i]), double(landmarks[i+1]));
        points.push_back(point);
    }

    cout << "Total of " << points.size() << " points\n";

    return points;
}

double distanceBetweenPoints(Point2f point1, Point2f point2)
{
    double distance = sqrt(pow((double) point1.x - (double) point2.x, 2) + pow((double) point1.y - (double) point2.y, 2));
    return distance;
}

double distanceFromCenter(Rect rect1, Point point1)
{
    /*
        Devuelve la distancia normalizada entre el centro de un Rect y un punto.
    */
    Point center = Point(rect1.x + (rect1.width / 2), rect1.y + (rect1.height / 2));
    double distance = sqrt(pow((double) center.x - (double) point1.x, 2) + pow((double) center.y - (double) point1.y, 2));
    return distance;
}

double distanceLineToPoint(Point2f line1, Point2f line2, Point point)
{

    double px = line2.x - line1.x;
    double py = line2.y - line1.y;

    double something = px * px + py * py;

    double u = ((point.x - line1.x) * px + (point.y - line1.y) * py) / something;

    if (u > 1)
    {
        u = 1;
    } else
    {
        u = 0;
    }

    double x = line1.x + u * px;
    double y = line1.y + u * py;

    double dx = x - point.x;
    double dy = y - point.y;

    return sqrt(dx*dx + dy*dy);
}
