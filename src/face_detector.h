#ifndef FACE_DETECTOR_H_
#define FACE_DETECTOR_H_

#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include <iostream>
#include <algorithm>
#include <cmath>

#include "flandmark_detector.h"

using namespace cv;
using namespace std;

double distanceFromCenter(Rect rect1, Point point1);
double distanceBetweenPoints(Point2f point1, Point2f point2);
double distanceLineToPoint(Point2f line1, Point2f line2, Point point);

// Estructura para hacer sort de los Rect por proximidad a un punto.
struct Local {
    Local(Point point) { this->point = point; }
    bool operator () (Rect rect1, Rect rect2) {
        double distance1 = distanceFromCenter(rect1, point);
        double distance2 = distanceFromCenter(rect2, point);
        return distance1 < distance2;
    }

   Point point;
};

class FaceDetector
{
    vector<CascadeClassifier> face_cascades;
    vector<CascadeClassifier> eyes_cascades; // unused
    vector<CascadeClassifier> mouth_cascades; // unused

    FLANDMARK_Model* model;
public:
    FaceDetector(vector<CascadeClassifier> f_c)
    {
        face_cascades = f_c;
        loadFlandmarkModel();
    };
    FaceDetector(vector<CascadeClassifier> f_c, vector<CascadeClassifier> e_c)
    {
        face_cascades = f_c;
        eyes_cascades = e_c;
        loadFlandmarkModel();
    };
    FaceDetector(vector<CascadeClassifier> f_c, vector<CascadeClassifier> e_c, vector<CascadeClassifier> m_c)
    {
        face_cascades = f_c;
        eyes_cascades = e_c;
        mouth_cascades = m_c;
        loadFlandmarkModel();
    };
    ~FaceDetector() {};
    bool getFace(Mat img, Point2f tag_f, int num_tags, Rect &face);
    int centerFace(Mat img, Rect &face, Mat &res, vector<Point2f*> features, vector<Point2f*> &rotatedFace);
    int drawFeatures(Mat img, vector<Point> eyes, vector<Rect> mouth, Mat &drawed);
    vector<Point2f*> detectFeatures(Mat img, Rect face);

private:
    void loadFlandmarkModel()
    {
        model = flandmark_init("flandmark_model.dat");
    }
    int findFaces(Mat img_grey, vector<Rect> &faces);
    int bestFace(vector<Rect> faces, Point tag, Size size, Rect &face);
    int findEyesMouth(Mat img_grey, Rect face, Point reshapePoint, vector<Point> &eyes, vector<Rect> &mouth);
    int loadCascade(vector<string> cascade_names, vector<CascadeClassifier> &cascades);
    int selectTwoEyes(Rect face, Point reshapePoint, vector<Rect> eyes_vector, vector<Point> &eyes);
    Point findPseudoCentroid(vector<Rect> rects);
    Mat getRotationMatrix(Point2f rightEye, Point2f leftEye, double scale);
    // Rect auxiliary functions
    int sortByDistance(vector<Rect> &faces, Point tag);
    double minSharedArea(Rect rect1, Rect rect2, Rect intersection);
    int intersect(Rect rect1, Rect rect2, Rect &intersection);
    bool validFace(Rect face, Point tag);
    void transformFeatures(Mat transformation, vector<Point2f*> features);
    void transformRect(Mat transformation, Rect &rect, vector<Point2f*> &rotated_rect);
};


#endif //FACE_DETECTOR_H_

