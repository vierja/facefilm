#include "fb.h"

#include "curl_easy.h"
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include <boost/lexical_cast.hpp>


std::string FB::getTaggedImagesUrl()
{

    std::ostringstream urlStream;
    urlStream << "https://graph.facebook.com/v2.0/fql?access_token=";
    urlStream << access_token;
    urlStream << "&format=json&method=get&pretty=0&q=select%20object_id%2C%20xcoord%2C%20ycoord%20from%20photo_tag%20where%20subject%3D";
    urlStream << user_id;

    return urlStream.str();
};

std::string FB::getObjectUrl(uint64_t object_id)
{
    std::ostringstream urlStream;
    urlStream << "https://graph.facebook.com/v2.1/";
    urlStream << object_id;
    urlStream << "?access_token=";
    urlStream << access_token;
    urlStream << "&format=json&method=get&suppress_http_code=1";

    return urlStream.str();
};

std::string FB::getUrl(std::string url)
{

    std::ostringstream response_buffer;
    curl::curl_writer writer(response_buffer);
    curl::curl_easy request(writer);
    request.add(curl_pair<CURLoption,string>(CURLOPT_URL, url));
     try {
         request.perform();
     } catch (curl_easy_exception error) {
         vector<pair<string,string>> errors = error.what();
         error.print_traceback();
         throw FB::FBError;
    }

    return response_buffer.str();
};

void FB::getImagesInfo(int worker)
{

    std::string str_user_id = boost::lexical_cast<std::string>(this->user_id);

    for (int i = 0; i < images.size(); i++)
    {
        if (images[i]->setFetching())
        {
            std::string response;

            int max_errors = 10;
            int errors = 0;
            while (errors < max_errors) {
                try {
                    response = getUrl(getObjectUrl(images[i]->object_id));
                    break;
                } catch (FB::FacebookError error) {
                    std::cout << "Error fetching image " << images[i]->object_id << "\n";
                    errors++;
                }
            }

            if (errors == max_errors)
            {
                images[i]->setError();
                continue;
            } 


            rapidjson::Document d;
            d.Parse(response.c_str());

            auto created_time = d["created_time"].GetString();
            auto source = d["source"].GetString();
            auto width = d["width"].GetInt();
            auto height = d["height"].GetInt();

            rapidjson::Value& tags = d["tags"]["data"];
            auto total_tags = tags.Size();

            // Save list of friends
            std::vector<TaggedFriend*> tagged_friends;
            for (auto i = 0 ; i < total_tags ; i++)
            {
                // Could happen if not friends
                if (!tags[i].HasMember("id") && !tags[i].HasMember("name"))
                {
                    continue;
                }

                auto friend_ = new TaggedFriend();

                if (tags[i].HasMember("id"))
                {
                    friend_->user_id = std::string(tags[i]["id"].GetString());
                }

                if (tags[i].HasMember("name"))
                {
                    friend_->name = std::string(tags[i]["name"].GetString());
                }

                if (friend_->user_id == str_user_id)
                {
                    //Ignoring auto user.
                    continue;
                }

                tagged_friends.push_back(friend_);
            }

            images[i]->tagged_friends = tagged_friends;
            images[i]->total_tags = total_tags;
            images[i]->created_time = created_time;
            images[i]->source = source;
            images[i]->width = width;
            images[i]->height = height;

            std::string image;

            max_errors = 10;
            errors = 0;
            while (errors < max_errors) {
                try {
                    image = getUrl(source);
                    break;
                } catch (FB::FacebookError error) {
                    std::cout << "Error fetching raw image " << images[i]->object_id << "\n";
                    errors++;
                }
            }

            if (errors == max_errors)
            {
                images[i]->setError();
                continue;
            }

            images[i]->image = image;

            images[i]->setFetched();
        }
    }
};

std::vector<Image*> FB::getTaggedImages(int num_threads, boost::thread_group *group, std::vector<uint64_t> selected_photos)
{

    std::string response = getUrl(getTaggedImagesUrl());
    std::cout << response << "\n";

    rapidjson::Document d;
    d.Parse(response.c_str());

    if (!d.HasMember("data"))
    {
        std::cout << "FB Token expired.\n";
        throw FB::FBError;
    }

    rapidjson::Value& data = d["data"];
    auto max_size = data.Size(); max_size--;
    std::cout << "max_size:" << max_size;
    std::cout << " data[max_size]: " << data[max_size]["object_id"].GetString() << "\n";
    for (auto i = max_size ; i-- > 0 ; )
    {
        auto object_id = (uint64_t) atol(data[i]["object_id"].GetString());

        if (selected_photos.size() > 0 && std::find(selected_photos.begin(), selected_photos.end(), object_id) == selected_photos.end())
        {
            /*
                If using selected photos, if we cannot find the object_id in the list. Then we ignore the image.
            */
            continue;
        }

        auto xcoord = data[i]["xcoord"].GetDouble();
        auto ycoord = data[i]["ycoord"].GetDouble();
        auto newImage = new Image(object_id, xcoord, ycoord);
        images.push_back(newImage);
    }

    std::cout << "Creating " << num_threads << " threads.\n";

    for (int i = 0; i < num_threads; i++)
    {
        boost::thread* thr = new boost::thread(boost::bind(&FB::getImagesInfo, this, i));
        group->add_thread(thr);
    }

    return images;
};