#ifndef FB_H_
#define FB_H_

#include <string>
#include <vector>
#include <exception>
#include <boost/thread/thread.hpp>
#include "image.h"


class FB
{

    class FacebookError: public std::exception
    {
        virtual const char* what() const throw()
        {
            return "Error connecting to FB.";
        }
    } FBError;

    std::string access_token;
    uint64_t user_id;
    std::vector<Image*> images;

    std::string getTaggedImagesUrl();
    std::string getObjectUrl(uint64_t object_id);
    std::string getUrl(std::string url);

    void getImagesInfo(int worker);

    public:
        FB(std::string token, uint64_t user) { access_token = token; user_id = user; };
        ~FB() {};
        std::vector<Image*> getTaggedImages(int num_threads, boost::thread_group *group, std::vector<uint64_t> selected_photos);
};

#endif // FB_H_