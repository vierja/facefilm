#include "fd.h"
#include "face_detector.h"
#include <algorithm>
#include <boost/date_time/posix_time/posix_time.hpp> // sleep
#include <boost/timer.hpp> // timer
#include <boost/lexical_cast.hpp> // casting
#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"


FDetection::FDetection(std::vector<Image*> im, std::vector<std::string> fc_str, std::string videoKey, redispp::Connection* conn, bool save_to_redis)
{
    this->images = im;
    this->face_cascades = fc_str;
    //this->eyes_cascades = ec_str;
    this->conn = conn;
    this->save_to_redis = save_to_redis;
    this->videoKey = videoKey;

};

void FDetection::findFaces(int num_threads, boost::thread_group *group)
{
    std::cout << "Find faces" << std::endl;
    std::cout << "Creating " << num_threads << " threads for finding them." << std::endl;

    for (int i = 0; i < num_threads; ++i)
    {
        boost::thread* thr = new boost::thread(boost::bind(&FDetection::detectFaces, this, i));
        group->add_thread(thr);
    }
};


void FDetection::detectFaces(int workerId)
{
    std::cout << "Starting worker " << workerId << " for detecting faces." << std::endl;
    std::vector<cv::CascadeClassifier> loaded_face_cascades;
    std::vector<cv::CascadeClassifier> loaded_eyes_cascades;

    for (std::vector<std::string>::iterator it = face_cascades.begin(); it != face_cascades.end(); ++it)
    {
        cv::CascadeClassifier cc;
        if (!cc.load(((std::string)*it).c_str()) ) {
            cout << "Error: Couldn't load cascade: " << *it << ".\n";
        } else {
            loaded_face_cascades.push_back(cc);
        }
    }

    for (std::vector<std::string>::iterator it = eyes_cascades.begin(); it != eyes_cascades.end(); ++it)
    {
        cv::CascadeClassifier cc;
        if (!cc.load(((std::string)*it).c_str()) ) {
            cout << "Error: Couldn't load cascade: " << *it << ".\n";
        } else {
            loaded_eyes_cascades.push_back(cc);
        }
    }

    if (loaded_face_cascades.size() == 0)
    {
        std::cout << "Could not load any face cascades." << std::endl;
        return;
    }

    auto face_detector = FaceDetector(loaded_face_cascades, loaded_eyes_cascades);

    for (int i = 0; i < images.size(); ++i)
    {
        while (!images[i]->readyToDetect())
        {
            boost::this_thread::sleep(boost::posix_time::milliseconds(40));
        }

        if (images[i]->setInProgress())
        {
            std::cout << "Processing image " << images[i]->object_id << "\n";

            boost::timer t;

            if (this->conn != NULL)
            {
                this->mtx_.lock();
                this->conn->hincrBy(this->videoKey, "fd_progress", 1);
                this->mtx_.unlock();
            }

            if (images[i]->hasErrorStatus())
            {
                std::cout << "Image " << images[i]->object_id << " has error status.\n";
                continue;
            }

            try
            {

                auto buf = images[i]->image.c_str();
                std::vector<char> data(buf, buf + images[i]->image.size());
                cv::Mat img_read = cv::imdecode(cv::Mat(data), CV_LOAD_IMAGE_COLOR);
                cv::Mat img(img_read.rows, img_read.cols, CV_32FC4);
                cv::cvtColor(img_read, img, COLOR_BGR2BGRA, 4);

                if (img.empty())
                {
                    std::cout << "Error: Image " << images[i]->object_id << " cannot be loaded." << std::endl;
                    continue;
                }

                // La funcion getFace devuelve Cara, y (opcional) ojos y bocas.
                cv::Rect face_rect;
                std::vector<cv::Point> eyes;
                std::vector<cv::Rect> mouth;
                bool res = false;

                cv::Point tag = cv::Point2f(images[i]->tag_x, images[i]->tag_y);

                std::cout << "Searching for face for " << images[i]->object_id << "\n";
                res = face_detector.getFace(img, tag, images[i]->total_tags, face_rect);
                std::cout << "Finish search for " << images[i]->object_id << "\n";
                if (res)
                {
                    std::vector<cv::Point2f*> features = face_detector.detectFeatures(img, face_rect);

                    if (features[0]->x != 0 && features[0]->y != 0)
                    {
                        /*
                        cv::Mat drawfeatures;

                        img.copyTo(drawfeatures);
                        for (auto point = features.begin(); point != features.end(); ++point)
                        {
                            auto color = Scalar(255, 255, 255);

                            circle(drawfeatures, **point, cvRound(1), color, 4, 8, 0);
                        }
                        */

                        Mat centered_img;

                        /*
                            Consideraciones a tener en cuenta.

                            El centrado de la imagen consiste en tres potenciales distintos movimientos.

                            > Traslacion
                                Llevar el centro de la cara desde el punto actual al centro de la imagen. (1280/2, 720/2)

                            > Rotacion
                                Alinear los ojos encontrados (centro de cada par de puntos para cada ojo) con una recta
                                donde y es constante (horizontal)

                            > Escala
                                Escalar el tamaño de la cara en base a:
                                    - Tamaños del rectangulo encontrado (definiendo una cara "ideal").
                                    - Distancia entre ojos, distancia entre ojos y boca, mezcla entre estos.


                            El procedimiento encontrado en esta parte del algoritmo es encarada SOLO de esto, y no 
                            de las modificaciones necesarias para hacer el morphing.

                            Idealmente este codigo es parte del video generator, de manera de poder realizar esta y las demas modifcaciones
                            en un mismo paso si tener que mantener todo en memoria durante la generacion del video.
                        */

                        vector<Point2f*> centered_face;
                        res = face_detector.centerFace(img, face_rect, centered_img, features, centered_face);
                        std::cout << "centered_face.size():" << centered_face.size() << "\n";

                        //imwrite(boost::lexical_cast<std::string>(images[i]->object_id).append("-2.jpg"), centered_img);

                        if (res)
                        {
                            double elapsed_time = t.elapsed();
                            std::cout << "Found " << images[i]->object_id << ": " << "(in " << elapsed_time << ")\n" << images[i]->source << std::endl;
                            images[i]->centered_img = centered_img;
                            images[i]->features = features;
                            images[i]->centered_face = centered_face;

                            images[i]->setFound();

                            if (this->conn != NULL)
                            {
                                this->mtx_.lock();
                                this->conn->hincrBy(this->videoKey, "found_face", 1);
                                this->mtx_.unlock();

                                if (this->save_to_redis)
                                {
                                    this->saveToRedis(images[i], face_rect, "found");
                                }
                            }
                            // Continue to avoid "setNotFound if we don't reach this code."
                            continue;                            
                        }
                    }
                }

                double elapsed_time = t.elapsed();
                std::cout << "Not Found " << images[i]->object_id << ": " << "(in " << elapsed_time << ")\n" << images[i]->source << std::endl;
                images[i]->setNotFound();
                if (this->conn != NULL)
                {
                    this->mtx_.lock();
                    this->conn->hincrBy(this->videoKey, "not_found_face", 1);
                    this->mtx_.unlock();

                    if (this->save_to_redis)
                    {
                        this->saveToRedis(images[i], face_rect, "not_found");
                    }
                }
            }
            catch( cv::Exception& e )
            {
                cout << "Exception finding face" << endl;
                images[i]->setNotFound();
                if (this->conn != NULL)
                {
                    this->mtx_.lock();
                    this->conn->hincrBy(this->videoKey, "error_face", 1);
                    this->mtx_.unlock();
                }
                continue;
            }
        }
    }
};

void FDetection::saveToRedis(Image* detected_image, cv::Rect face_rect, std::string image_status)
{
    if (this->conn != NULL && this->save_to_redis)
    {
        try
        {

            rapidjson::Document d;
            d.SetObject();
            rapidjson::Document::AllocatorType& allocator = d.GetAllocator();
            rapidjson::Value v;
            v.SetString(detected_image->source.c_str(), allocator);
            d.AddMember("src", v, allocator);
            v.SetString(image_status.c_str(), allocator);
            d.AddMember("status", v, allocator);
            d.AddMember("width", detected_image->width, allocator);
            d.AddMember("height", detected_image->height, allocator);
            d.AddMember("face_x", face_rect.x, allocator);
            d.AddMember("face_y", face_rect.y, allocator);
            d.AddMember("face_width", face_rect.width, allocator);
            d.AddMember("face_height", face_rect.height, allocator);
            d.AddMember("object_id", detected_image->object_id, allocator);

            rapidjson::Value friends_array(rapidjson::kArrayType);

            auto tagged_friends = detected_image->tagged_friends;

            for(auto it = tagged_friends.begin(); it != tagged_friends.end(); ++it) {
                
                TaggedFriend* tagged_friend = (TaggedFriend*) (*it);
   
                rapidjson::Value objValue;
                objValue.SetObject();

                if (tagged_friend->user_id.size() > 0)
                {
                    rapidjson::Value userValue;
                    userValue.SetString(tagged_friend->user_id.c_str(), allocator);
                    objValue.AddMember("user_id", userValue, allocator);
                }

                if (tagged_friend->name.size() > 0)
                {
                    rapidjson::Value nameValue;
                    nameValue.SetString(tagged_friend->name.c_str(), allocator);
                    objValue.AddMember("name", nameValue, allocator);
                }

                friends_array.PushBack(objValue, allocator);
            }

            d.AddMember("tagged_friends", friends_array, allocator);

            rapidjson::StringBuffer buffer;
            rapidjson::Writer<rapidjson::StringBuffer> writer(buffer);
            d.Accept(writer);

            auto json_string = buffer.GetString();

            this->mtx_.lock();
            try
            {
                this->conn->lpush(this->videoKey + ":faces", json_string);
                this->conn->publish(this->videoKey + ":faces_channel", json_string);
                this->mtx_.unlock();
            } catch (...)
            {
                std::cout << "Error pushing json to redis. \n";
                this->mtx_.unlock();
            }
            

        } catch (...)
        {
            std::cout << "Error saving image json to redis.\n";
        }

    }
};