#ifndef FD_H_
#define FD_H_

#include <string>
#include <vector>
#include <exception>
#include <boost/thread/thread.hpp>
#include "opencv2/objdetect/objdetect.hpp"
#include "image.h"
#include <redispp.h>

class FDetection
{

    std::vector<Image*> images;
    std::vector<std::string> face_cascades;
    std::vector<std::string> eyes_cascades;
    std::vector<cv::FileStorage> loaded_face_cascades;
    std::string videoKey;
    redispp::Connection* conn;
    bool save_to_redis;
    boost::mutex mtx_;

    void detectFaces(int workerId);

    public:
        FDetection(std::vector<Image*> images, std::vector<std::string> face_cascades, std::string videoKey, redispp::Connection* conn, bool save_to_redis);
        ~FDetection() {};
        void findFaces(int num_threads, boost::thread_group *group);
        void saveToRedis(Image* detected_image, cv::Rect face_rect, std::string image_status);
};

#endif // FD_H_