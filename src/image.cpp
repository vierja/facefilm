#include "image.h"

Image::~Image() {}

Image::Image(const Image& im)
{
    tag_x = im.tag_x;
    tag_y = im.tag_y;
    object_id = im.object_id;
    total_tags = im.total_tags;
    created_time = im.created_time;
    source = im.source;
    width = im.width;
    height = im.height;
    image = im.image;
    status = im.status;
}

bool Image::setFetching()
{
    boost::lock_guard<boost::mutex> guard(mtx_);

    if (status == PENDING)
    {
        status = FETCHING;
        return true;
    }

    return false;
}

bool Image::setFetched()
{
    boost::lock_guard<boost::mutex> guard(mtx_);

    if (status == FETCHING)
    {
        status = FETCHED;
        return true;
    }

    return false;
}

bool Image::setError()
{
    boost::lock_guard<boost::mutex> guard(mtx_);

    if (status == FETCHING || status == IN_PROCESS)
    {
        status = ERROR;
        return true;
    }

    return false;
}

bool Image::readyToDetect()
{
    boost::lock_guard<boost::mutex> guard(mtx_);
    return status != PENDING && status != FETCHING;
}

bool Image::setInProgress()
{
    boost::lock_guard<boost::mutex> guard(mtx_);

    if (status == FETCHED)
    {
        status = IN_PROCESS;
        return true;
    }

    if (status == ERROR)
    {
        status = ERROR_FD;
        return true;
    }

    return false;
}

bool Image::setNotFound()
{
    boost::lock_guard<boost::mutex> guard(mtx_);

    if (status == IN_PROCESS)
    {
        status = NOT_FOUND;
        return true;
    }

    return false;
}

bool Image::hasErrorStatus()
{
    boost::lock_guard<boost::mutex> guard(mtx_);
    return status == ERROR_FD || status == ERROR;
}

bool Image::setFound()
{
    boost::lock_guard<boost::mutex> guard(mtx_);

    if (status == IN_PROCESS)
    {
        status = FOUND;
        return true;
    }

    return false;
}

bool Image::readyToVideo()
{
    boost::lock_guard<boost::mutex> guard(mtx_);
    return status == FOUND || status == NOT_FOUND;
}

bool Image::isFound()
{
    boost::lock_guard<boost::mutex> guard(mtx_);
    return status == FOUND;
}

// void Image::morph(cv::Mat mImage, std::vector<cv::Point2f*> mFeatures, double percentage /* 0 <= p <= 1*/, cv::Mat &resultingImage)
// {
    
// }

/*
New morphing, using pre-defined sets of points.
*/
std::vector<std::vector<cv::Point2f *>> getFacePointSets(cv::Mat image, std::vector<cv::Point2f *> face, std::vector<cv::Point2f *> features)
{
    std::vector<std::vector<cv::Point2f *>> sets;

    // 11 sets distintos.

    /*
        Always clockwise starting top left.
    */
    std::vector<cv::Point2f*> figure;
    cv::Point2f* p1;
    cv::Point2f* p2;
    cv::Point2f* p3;
    cv::Point2f* p4;

    // 1
    p1 = new cv::Point2f(*face[0]);
    figure.push_back(p1);
    p2 = new cv::Point2f(*face[1]);
    figure.push_back(p2);
    p3 = new cv::Point2f(*features[1]);
    figure.push_back(p3);
    p4 = new cv::Point2f(*features[5]);
    figure.push_back(p4);
    sets.push_back(figure);
    figure.clear();

    // 2
    p1 = new cv::Point2f(*face[0]);
    figure.push_back(p1);
    p2 = new cv::Point2f(*features[5]);
    figure.push_back(p2);
    p3 = new cv::Point2f(*face[3]);
    figure.push_back(p3);
    sets.push_back(figure);
    figure.clear();

    // 3
    p1 = new cv::Point2f(*features[5]);
    figure.push_back(p1);
    p2 = new cv::Point2f(*features[3]);
    figure.push_back(p2);
    p3 = new cv::Point2f(*face[3]);
    figure.push_back(p3);
    sets.push_back(figure);
    figure.clear();

    // 4
    p1 = new cv::Point2f(*features[5]);
    figure.push_back(p1);
    p2 = new cv::Point2f(*features[1]);
    figure.push_back(p2);
    p3 = new cv::Point2f(*features[7]);
    figure.push_back(p3);
    p4 = new cv::Point2f(*features[3]);
    figure.push_back(p4);
    sets.push_back(figure);
    figure.clear();

    // 5
    p1 = new cv::Point2f(*features[7]);
    figure.push_back(p1);
    p2 = new cv::Point2f(*features[4]);
    figure.push_back(p2);
    p3 = new cv::Point2f(*features[3]);
    figure.push_back(p3);
    sets.push_back(figure);
    figure.clear();

    // 6
    p1 = new cv::Point2f(*features[3]);
    figure.push_back(p1);
    p2 = new cv::Point2f(*features[4]);
    figure.push_back(p2);
    p3 = new cv::Point2f(*face[2]);
    figure.push_back(p3);
    p4 = new cv::Point2f(*face[3]);
    figure.push_back(p4);
    sets.push_back(figure);
    figure.clear();

    // 7
    p1 = new cv::Point2f(face[0]->x + (face[1]->x - face[0]->x)/2, face[0]->y);
    figure.push_back(p1);
    p2 = new cv::Point2f(*features[2]);
    figure.push_back(p2);
    p3 = new cv::Point2f(*features[7]);
    figure.push_back(p3);
    p4 = new cv::Point2f(*features[1]);
    figure.push_back(p4);
    sets.push_back(figure);
    figure.clear();

    // 8
    p1 = new cv::Point2f(*features[2]);
    figure.push_back(p1);
    p2 = new cv::Point2f(*features[6]);
    figure.push_back(p2);
    p3 = new cv::Point2f(*features[4]);
    figure.push_back(p3);
    p4 = new cv::Point2f(*features[7]);
    figure.push_back(p4);
    sets.push_back(figure);
    figure.clear();

    // 9
    p1 = new cv::Point2f(*features[6]);
    figure.push_back(p1);
    p2 = new cv::Point2f(*face[2]);
    figure.push_back(p2);
    p3 = new cv::Point2f(*features[4]);
    figure.push_back(p3);
    sets.push_back(figure);
    figure.clear();

    // 10
    p1 = new cv::Point2f(*face[1]);
    figure.push_back(p1);
    p2 = new cv::Point2f(*face[2]);
    figure.push_back(p2);
    p3 = new cv::Point2f(*features[6]);
    figure.push_back(p3);
    sets.push_back(figure);
    figure.clear();

    // 11
    p1 = new cv::Point2f(face[0]->x + (face[1]->x - face[0]->x)/2, face[0]->y);
    figure.push_back(p1);
    p2 = new cv::Point2f(*face[1]);
    figure.push_back(p2);
    p3 = new cv::Point2f(*features[6]);
    figure.push_back(p3);
    p4 = new cv::Point2f(*features[2]);
    figure.push_back(p4);
    sets.push_back(figure);
    figure.clear();

    // 12
    p1 = new cv::Point2f(0,0);
    figure.push_back(p1);
    p2 = new cv::Point2f(image.cols, 0);
    figure.push_back(p2);
    p3 = new cv::Point2f(*face[1]);
    figure.push_back(p3);
    p4 = new cv::Point2f(*face[0]);
    figure.push_back(p4);
    sets.push_back(figure);
    figure.clear();

    // 13
    p1 = new cv::Point2f(*face[1]);
    figure.push_back(p1);
    p2 = new cv::Point2f(image.cols, 0);
    figure.push_back(p2);
    p3 = new cv::Point2f(image.cols, image.rows);
    figure.push_back(p3);
    p4 = new cv::Point2f(*face[2]);
    figure.push_back(p4);
    sets.push_back(figure);
    figure.clear();

    // 14
    p1 = new cv::Point2f(*face[3]);
    figure.push_back(p1);
    p2 = new cv::Point2f(*face[2]);
    figure.push_back(p2);
    p3 = new cv::Point2f(image.cols, image.rows);
    figure.push_back(p3);
    p4 = new cv::Point2f(0, image.rows);
    figure.push_back(p4);
    sets.push_back(figure);
    figure.clear();

    // 15
    p1 = new cv::Point2f(0, 0);
    figure.push_back(p1);
    p2 = new cv::Point2f(*face[0]);
    figure.push_back(p2);
    p3 = new cv::Point2f(*face[3]);
    figure.push_back(p3);
    p4 = new cv::Point2f(0, image.rows);
    figure.push_back(p4);
    sets.push_back(figure);
    figure.clear();


    return sets;
}
