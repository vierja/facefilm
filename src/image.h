#ifndef IMAGE_H_
#define IMAGE_H_

#include <string>
#include <vector>
#include <boost/thread/mutex.hpp>
#include <boost/thread/locks.hpp>
#include <boost/atomic.hpp>
#include "opencv2/objdetect/objdetect.hpp" // cv::Mat


struct TaggedFriend {
    std::string user_id;
    std::string name;
};

std::vector<std::vector<cv::Point2f *>> getFacePointSets(cv::Mat image, std::vector<cv::Point2f*> face, std::vector<cv::Point2f *> features);

class Image
{
    enum ImageStatus { PENDING, FETCHING, FETCHED, IN_PROCESS, NOT_FOUND, FOUND, ERROR, ERROR_FD };

    public:

    double tag_x;
    double tag_y;
    uint64_t object_id;
    std::vector<TaggedFriend*> tagged_friends;
    int total_tags;
    std::string created_time;
    std::string source;
    int width;
    int height;
    std::string image;
    cv::Mat centered_img;
    std::vector<cv::Point2f *> features;
    std::vector<cv::Point2f *> centered_face;

    Image::ImageStatus status = PENDING;

    boost::mutex mtx_;

    Image(uint64_t oid, double tagX, double tagY) { tag_x = tagX; tag_y = tagY; object_id = oid; };
    Image(const Image&);
    ~Image();
    bool setFetching();
    bool setFetched();
    bool setError();
    bool readyToDetect();
    bool setInProgress();
    bool setNotFound();
    bool setFound();
    bool readyToVideo();
    bool isFound();
    bool hasErrorStatus();

    void morph(cv::Mat mImage, std::vector<cv::Point2f*> mFeatures, double percentage /* 0 <= p <= 1*/, cv::Mat &resultingImage);


    bool operator < (const Image& image2) const
    {
        return (created_time < image2.created_time);
    }

    Image& operator= (const Image& other)
    {
        if (this != &other) 
        {
            tag_x = other.tag_x;
            tag_y = other.tag_y;
            object_id = other.object_id;
            total_tags = other.total_tags;
            created_time = other.created_time;
            source = other.source;
            width = other.width;
            height = other.height;
            image = other.image;
            centered_img = other.centered_img;
            status = other.status;
        }
        // by convention, always return *this
        return *this;
    }

    friend void swap(Image& a, Image& b)
    {
        using std::swap; // bring in swap for built-in types

        swap (a.tag_x, b.tag_x);
        swap (a.tag_y, b.tag_y);
        swap (a.object_id, b.object_id);
        swap (a.total_tags, b.total_tags);
        swap (a.created_time, b.created_time);
        swap (a.source, b.source);
        swap (a.width, b.width);
        swap (a.height, b.height);
        swap (a.image, b.image);
        swap (a.centered_img, b.centered_img);
        swap (a.status, b.status);

    }

};

#endif