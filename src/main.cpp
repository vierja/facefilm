#include <vector>
#include <sstream>
#include <iostream>
#include <fstream>
#include <algorithm>
#include <stdlib.h>

#include <boost/thread/thread.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/atomic.hpp>
#include <boost/format.hpp>
#include <boost/program_options.hpp>
#include <boost/timer.hpp> // timer

#include "yaml-cpp/yaml.h"

#include "rapidjson/document.h"
#include "rapidjson/writer.h"
#include "rapidjson/stringbuffer.h"

#include <redispp.h>

#include "image.h"
#include "fb.h"
#include "fd.h"
#include "vg.h"

// TODO: Loaded from config file.
static const char * face_cascades[] = {
    "cascades/lbpcascade_frontalface.xml",
    "cascades/haarcascade_frontalface_default.xml",
    "cascades/haarcascade_frontalface_alt2.xml",
    "cascades/haarcascade_frontalface_alt.xml",
    "cascades/haarcascade_frontalface_alt_tree.xml"
};

namespace po = boost::program_options;

std::vector<uint64_t> load_selected_photos(redispp::Connection* conn, std::string videoKey);

/**
 * ./main 12312312 video.avi access_token
 * ./main <videoId>
 */
int main(int argc, char* argv[])
{
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help", "produce help message")
        ("redis", po::value<std::string>(), "set redis server hostname")
        ("video-id", po::value<std::string>(), "set videoId")
        ("user-id", po::value<long>(), "set userId")
        ("video", po::value<std::string>(), "set video filename")
        ("token", po::value<std::string>(), "set user access token")
        ("only-fd", po::value<bool>(), "set for only doing face detection")
        ("load-photos", po::value<bool>(), "set for loading allowed photos_ids from redis")
    ;
    po::variables_map vm;    

    try
    {  
        po::store(po::parse_command_line(argc, argv, desc), vm);
        po::notify(vm);
    } catch (...)
    {
        std::cout << "Error parsing options. \n" << desc << "\n";  
    }

    if (vm.count("help"))
    {
        std::cout << desc << "\n";
        return 1;
    }

    std::string redis_host;
    if (vm.count("redis"))
    {
        redis_host = vm["redis"].as<std::string>();
    }

    std::string videoId;
    redispp::Connection* conn = NULL;
    std::string videoKey;
    std::string strUserId;
    long userId;
    std::string videoLoc;
    std::string accessToken;
    bool only_fd = false;
    std::vector<uint64_t> selected_photos;

    int video_fps = -1;
    int video_transition_frames = -1;
    int video_wait_frames = -1;

    if (vm.count("only-fd"))
    {
        std::cout << "Mode: Only facedetection.\n";
        only_fd = true;
    }

    if (redis_host.size() > 0){

        if (vm.count("video-id"))
        {
            videoId = vm["video-id"].as<std::string>();
        } else
        {
            std::cout << "You must specify the videoId found in redis.\n";
            return -1;
        }

        try
        {
            conn = new redispp::Connection(std::string(redis_host), "6379", "", false);
        } catch (...)
        {
            std::cout << "Error connecting to redis on host '" << redis_host << "'\n";
            return -1;
        }

        videoKey = "video:" + videoId;

        try
        {
            strUserId = (std::string) conn->hget(videoKey, "user_id");
            userId = boost::lexical_cast<long>(strUserId);
            accessToken = (std::string) conn->hget(videoKey, "access_token");

            if (conn->hexists(videoKey, "video_name"))
            {
                videoLoc = (std::string) conn->hget(videoKey, "video_name");
            } else
            {
                videoLoc = strUserId + ".avi";
            }

            if (vm.count("load-photos"))
            {
                std::cout << "Mode: Using saved photos ids to load allowed images.\n";
                selected_photos = load_selected_photos(conn, videoKey);
                std::cout << "Loaded " << selected_photos.size() << " selected photos to use.\n";
            }

            if (conn->hexists(videoKey, "transition_frames"))
            {
                video_transition_frames = boost::lexical_cast<int>((std::string) conn->hget(videoKey, "transition_frames"));
            }

            if (conn->hexists(videoKey, "wait_frames"))
            {
                video_wait_frames = boost::lexical_cast<int>((std::string) conn->hget(videoKey, "wait_frames"));
            }

            if (conn->hexists(videoKey, "fps"))
            {
                video_fps = boost::lexical_cast<int>((std::string) conn->hget(videoKey, "fps"));
            }

        } catch (...)
        {
            std::cout << "Error reading values from redis with key: " << videoKey << ".\n";
            return 1;
        }

    } else
    {
        if (vm.count("user-id"))
        {
            userId = vm["user-id"].as<long>();
        } else
        {
            std::cout << "Without redis user-id is mandatory.\n";
            return -1;
        }

        if (vm.count("video"))
        {
            videoLoc = vm["video"].as<std::string>();
        } else
        {
            std::cout << "Without redis '--video' is mandatory.\n";
            return -1;
        }

        if (vm.count("token"))
        {
            accessToken = vm["token"].as<std::string>();
        } else
        {
            std::cout << "Without redis '--token' is mandatory.\n";
            return -1;
        }
    }

    // Parse config file.
    YAML::Node config;
    try
    {
        config = YAML::LoadFile("config.yml");
    } catch (YAML::BadFile e)
    {
        std::cout << "Could not load config.yml file.\n";
        return 1;
    }

    // Load video settings from config file.
    if (config["video"])
    {
        if (config["video"]["fps"] && video_fps == -1) video_fps = config["video"]["fps"].as<int>();
        if (config["video"]["transition_frames"] && video_transition_frames == -1) video_transition_frames = config["video"]["transition_frames"].as<int>();
        if (config["video"]["wait_frames"] && video_wait_frames == -1) video_wait_frames = config["video"]["wait_frames"].as<int>();

        if (config["video"]["video_folder"])
        {
            auto videoFolder = config["video"]["video_folder"].as<std::string>();
            videoLoc = videoFolder.append("/").append(videoLoc);
        }
    }

    // Hardcoded defaults just in case
    if (video_fps == -1) video_fps == 30;
    if (video_transition_frames == -1) video_transition_frames == 8;
    if (video_wait_frames == -1) video_wait_frames == 1;

    std::cout << "Video location: " << videoLoc << "\n";
    std::cout << "(fps: " << video_fps << ", transition_frames: " << video_transition_frames << ", wait_frames: " << video_wait_frames << ")\n";

    std::vector<std::string> face_cascade_names;

    // Load cascades names from config file.
    if (config["cascades"])
    {
        if (config["cascades"]["face"])
        {
            face_cascade_names = config["cascades"]["face"].as<std::vector<std::string>>();
        }
    }

    // Get shit done
    try
    {

        auto fb = FB(accessToken, userId);

        boost::thread_group* group = new boost::thread_group();
        std::vector<Image*> images = fb.getTaggedImages(10, group, selected_photos);
        std::cout << "Images size: " << images.size() << "\n";

        if (conn != NULL) conn->hset(videoKey, "total_images", std::to_string(images.size()));

        std::vector<std::string> face_cascade_names(face_cascades, face_cascades + sizeof(face_cascades) / sizeof(face_cascades[0]));
        boost::timer fd_t;
        auto fd = new FDetection(images, face_cascade_names, videoKey, conn, only_fd);
        fd->findFaces(1, group);

        if (!only_fd)
        {
            boost::timer vg_t;
            auto vg = VGenerator(images, video_fps, video_transition_frames, video_wait_frames, videoKey, conn);
            vg.createVideo(videoLoc);
            double vg_elapsed_time = vg_t.elapsed();

            if (conn != NULL) conn->hset(videoKey, "vg_elapsed_time", boost::lexical_cast<std::string>(vg_elapsed_time));
        }

        group->join_all();
        double fd_elapsed_time = fd_t.elapsed();
        if (conn != NULL && only_fd)
        {
            conn->hset(videoKey, "fd_elapsed_time", boost::lexical_cast<std::string>(fd_elapsed_time));
        }

        if (conn != NULL)
        {
            if (!only_fd)
            {
                conn->hset(videoKey, "status", "done");
            } else 
            {
                conn->hset(videoKey, "status", "done_faces");
            }
        }

    } catch( const std::exception & ex )
    {
        std::cout << "Error while executing face detection/video:" << ex.what() << "\n";
        if (conn != NULL)
        {
            conn->hset(videoKey, "status", "error");
        }
        return 1;
    }

    return 0;
}

std::vector<uint64_t> load_selected_photos(redispp::Connection* conn, std::string videoKey)
{

    auto allowed_photos = (std::string) conn->hget(videoKey, "allowed_photos");

    rapidjson::Document d;
    d.Parse(allowed_photos.c_str());

    if (!d.HasMember("photos"))
    {
        std::vector<uint64_t> selected_photos;
        return selected_photos;
    }

    rapidjson::Value& photos = d["photos"];

    std::vector<uint64_t> selected_photos(photos.Size(), 0);

    for (auto i = 0 ; i < photos.Size() ; i++)
    {
        selected_photos.push_back(photos[i].GetUint64());
    }

    return selected_photos;
}
