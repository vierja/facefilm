#include "vg.h"
#include "unistd.h"
#include <stdio.h>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

void VGenerator::createVideo(std::string videoLoc)
{
    std::cout << "Create video" << std::endl;

    if (!initVideoPipe(videoLoc, "ffmpeg"))
    {
        std::cout << "ERROR creating video." << std::endl;
        return;
    }

    cv::Mat lastImage;
    cv::Mat firstImage;
    std::vector<std::vector<cv::Point2f*>> lastSectors;

    bool firstImageFound = false;

    for (int i = 0; i < images.size(); i++)
    {
        while (!images[i]->readyToVideo())
        {
            boost::this_thread::sleep(boost::posix_time::milliseconds(40));
        }

        if (this->conn != NULL)
        {
            this->conn->hincrBy(this->videoKey, "vg_progress", 1);
        }

        if (images[i]->isFound())
        {
            cv::Mat image = images[i]->centered_img;

            /*
            // New Morphing
            std::vector<Sector> sectors = getFacePointSets(images[i]->centered_img, images[i]->centered_face, images[i]->features);

            std::cout << "VG: " << sectors.size() << " different sections\n";
            for (auto section = sectors.begin(); section != sectors.end(); ++section)
            {
                std::cout << "Section length: " << (*section).size() << "\n";
            }
            */

            if (!firstImageFound)
            {
                // Morph the image to itself.
                morph(image, image, lastImage);
                firstImageFound = true;
                firstImage = image;

                //New morphing
                /*
                lastSectors = sectors;
                lastImage = image;
                */

            } else
            {
                // Morph the new image to the last image in video.
                morph(lastImage, image, lastImage);

                // New morphing
                /*
                realMorph(lastImage, lastSectors, image, sectors, lastImage);
                lastSectors = sectors;
                */
            }
        }
    }

    // Fade to black
    morph(lastImage, lastImage, lastImage, 120, 1, true);

    pclose(pipe);
};

bool VGenerator::initVideoPipe(std::string videoLoc, std::string ffmpeg_location)
{
    //ffmpeg -y -f image2pipe
    std::string ffmpeg_command = ffmpeg_location.append(" -y -f image2pipe -vcodec mjpeg -r ")
                                                .append(boost::lexical_cast<std::string>(fps))
                                                .append(" -i - -vcodec libx264 -preset veryfast -crf 30 -r ")
                                                .append(boost::lexical_cast<std::string>(fps))
                                                .append(" -threads 0 ")
                                                .append(videoLoc);


    pipe = popen(ffmpeg_command.c_str(), "w");

    if (pipe == NULL)
    {
        std::cout << "Error: Failed to open ffmpeg pipe." << std::endl;
        return false;
    }

    return true;
};

bool VGenerator::morph(cv::Mat img1, cv::Mat img2, cv::Mat &lastFrame)
{
    morph(img1, img2, lastFrame, transition_frames, wait_frames, false);
}

bool VGenerator::morph(cv::Mat img1, cv::Mat img2, cv::Mat &lastFrame, int transition_frames, int wait_frames, bool to_black)
{

    double opacity_increment = 1.0 / transition_frames;
    double opacity = opacity_increment;

    int total_frames = transition_frames + wait_frames;

    for (int i = 0; i < total_frames; i++)
    {

        cv::Mat result;

        img1.copyTo(result);


        // addWeighted writes black over transparent sections
        //addWeighted(img1, (1-opacity), img2, opacity, 0.0, result);

        // start at the row indicated by location, or at row 0 if location.y is negative.
        for(int y = 0; y < img1.rows; ++y)
        for(int x = 0; x < img1.cols; x+=8)
        {
            if (to_black)
            {
                result.at<cv::Vec4b>(y,x) = (1-opacity) * img1.at<cv::Vec4b>(y,x);
                result.at<cv::Vec4b>(y,x+1) = (1-opacity) * img1.at<cv::Vec4b>(y,x+1);
                result.at<cv::Vec4b>(y,x+2) = (1-opacity) * img1.at<cv::Vec4b>(y,x+2);
                result.at<cv::Vec4b>(y,x+3) = (1-opacity) * img1.at<cv::Vec4b>(y,x+3);
                result.at<cv::Vec4b>(y,x+4) = (1-opacity) * img1.at<cv::Vec4b>(y,x+4);
                result.at<cv::Vec4b>(y,x+5) = (1-opacity) * img1.at<cv::Vec4b>(y,x+5);
                result.at<cv::Vec4b>(y,x+6) = (1-opacity) * img1.at<cv::Vec4b>(y,x+6);
                result.at<cv::Vec4b>(y,x+7) = (1-opacity) * img1.at<cv::Vec4b>(y,x+7);

            } else
            {
                if (img2.at<cv::Vec4b>(y, x)[3] != 0)
                {
                    result.at<cv::Vec4b>(y,x) = (1-opacity) * img1.at<cv::Vec4b>(y,x) + opacity * img2.at<cv::Vec4b>(y,x);
                }
                if (img2.at<cv::Vec4b>(y, x+1)[3] != 0)
                {
                    result.at<cv::Vec4b>(y,x+1) = (1-opacity) * img1.at<cv::Vec4b>(y,x+1) + opacity * img2.at<cv::Vec4b>(y,x+1);
                }
                if (img2.at<cv::Vec4b>(y, x+2)[3] != 0)
                {
                    result.at<cv::Vec4b>(y,x+2) = (1-opacity) * img1.at<cv::Vec4b>(y,x+2) + opacity * img2.at<cv::Vec4b>(y,x+2);
                }
                if (img2.at<cv::Vec4b>(y, x+3)[3] != 0)
                {
                    result.at<cv::Vec4b>(y,x+3) = (1-opacity) * img1.at<cv::Vec4b>(y,x+3) + opacity * img2.at<cv::Vec4b>(y,x+3);
                }
                if (img2.at<cv::Vec4b>(y, x+4)[3] != 0)
                {
                    result.at<cv::Vec4b>(y,x+4) = (1-opacity) * img1.at<cv::Vec4b>(y,x+4) + opacity * img2.at<cv::Vec4b>(y,x+4);
                }
                if (img2.at<cv::Vec4b>(y, x+5)[3] != 0)
                {
                    result.at<cv::Vec4b>(y,x+5) = (1-opacity) * img1.at<cv::Vec4b>(y,x+5) + opacity * img2.at<cv::Vec4b>(y,x+5);
                }
                if (img2.at<cv::Vec4b>(y, x+6)[3] != 0)
                {
                    result.at<cv::Vec4b>(y,x+6) = (1-opacity) * img1.at<cv::Vec4b>(y,x+6) + opacity * img2.at<cv::Vec4b>(y,x+6);
                }
                if (img2.at<cv::Vec4b>(y, x+7)[3] != 0)
                {
                    result.at<cv::Vec4b>(y,x+7) = (1-opacity) * img1.at<cv::Vec4b>(y,x+7) + opacity * img2.at<cv::Vec4b>(y,x+7);
                }
              
            }
        }

        // Aumento la opacity para la siguiente operacion.
        opacity = std::min(opacity + opacity_increment, 1.0);

        this->frameToVideo(result);

        // if last, save result.
        if (i == total_frames - 1)
        {
            lastFrame = result;
        }
    }

    return 1;
}

bool VGenerator::frameToVideo(cv::Mat frame)
{
    std::vector<uchar> buf;
    cv::imencode(".jpg", frame, buf);
    std::cout << "Encoded frame" << std::endl;

    return this->bytesToVideo(reinterpret_cast<char*> (&buf[0]), buf.size());
}

bool VGenerator::bytesToVideo(char* frameBytes, int size)
{
    if (this->pipe)
    {
        fwrite(frameBytes, sizeof(char), size, this->pipe);
        return true;
    }

    return false;
}

cv::Mat getNewSector(Sector startSector, Sector endSector, double percentage, cv::Mat image, int num)
{
    cv::Mat mask = cv::Mat::zeros(image.rows, image.cols, CV_8UC1);

    std::vector<cv::Point> altSector;
    for (auto point = startSector.begin(); point != startSector.end(); ++point)
    {
        cv::Point p = cv::Point((**point).x, (**point).y);
        altSector.push_back(p);
    }
    cv::fillConvexPoly(mask, &altSector[0], altSector.size(), 255);
    std::cout << "New mask\n";

    cv::Mat masked = cv::Mat::zeros(image.rows, image.cols, image.type());

    image.copyTo(masked, mask);

    cv::imwrite(boost::lexical_cast<std::string>(num).append(".jpg"), masked);

    cv::Mat transform;
    cv::Mat newSector = cv::Mat::zeros(image.rows, image.cols, image.type());

    if (startSector.size() == 3)
    {
        std::cout << "startSector.size():" << startSector.size() << ", endSector.size() " << endSector.size() << "\n";
        const cv::Point2f srcTri[3] = {cv::Point2f(startSector[0]->x, startSector[0]->y), cv::Point2f(startSector[1]->x, startSector[1]->y), cv::Point2f(startSector[2]->x, startSector[2]->y)};
        const cv::Point2f dstTri[3] = {cv::Point2f(endSector[0]->x, endSector[0]->y), cv::Point2f(endSector[1]->x, endSector[1]->y), cv::Point2f(endSector[2]->x, endSector[2]->y)};

        std::cout << "srcTri:" << srcTri[0] << "," << srcTri[1] << "," << srcTri[2] << "\n";
        std::cout << "dstTri:" << dstTri[0] << "," << dstTri[1] << "," << dstTri[2] << "\n";

        transform = getAffineTransform(srcTri, dstTri);
        std::cout << "warpAffine with transform:" << transform << "\n";
        warpAffine(masked, newSector, transform, newSector.size());
        std::cout << "Warped\n";


    } else if (startSector.size() == 4) {
        std::cout << "startSector.size():" << startSector.size() << ", endSector.size() " << endSector.size() << "\n";
        const cv::Point2f srcTri[4] = {cv::Point2f(startSector[0]->x, startSector[0]->y), cv::Point2f(startSector[1]->x, startSector[1]->y), cv::Point2f(startSector[2]->x, startSector[2]->y), cv::Point2f(startSector[3]->x, startSector[3]->y)};
        const cv::Point2f dstTri[4] = {cv::Point2f(endSector[0]->x, endSector[0]->y), cv::Point2f(endSector[1]->x, endSector[1]->y), cv::Point2f(endSector[2]->x, endSector[2]->y), cv::Point2f(endSector[3]->x, endSector[3]->y)};

        std::cout << "srcTri:" << srcTri[0] << "," << srcTri[1] << "," << srcTri[2] << "," << srcTri[3] << "\n";
        std::cout << "dstTri:" << dstTri[0] << "," << dstTri[1] << "," << dstTri[2] << "," << dstTri[3] << "\n";

        transform = getPerspectiveTransform(srcTri, dstTri);

        std::cout << "warpPerspective with transform:" << transform << "\n";
        warpPerspective(masked, newSector, transform, newSector.size());
        std::cout << "Warped\n";

    }

    return newSector;

}

void VGenerator::realMorph(cv::Mat startImage, std::vector<Sector> startSectors, cv::Mat endImage, std::vector<Sector> endSectors, cv::Mat &lastFrame)
{

    int count = 0;
    auto startSector = startSectors.begin();
    auto endSector = endSectors.begin();

    cv::Mat morphed = cv::Mat::zeros(startImage.rows, startImage.cols, startImage.type());

    std::cout << "startSectors.size(): " << startSectors.size() << ", endSectors.size(): " << endSectors.size() << "\n";

    while (startSector != startSectors.end())
    {

        std::cout << "Creating new sector with startSector and endSector\n";
        std::cout << "startSector.size(): " << (*startSector).size() << ", endSector.size(): " << (*endSector).size() << "\n";
        std::cout << "startSector[0]: " << (*startSector)[0] << ", *startSector[1]: " << (*startSector)[1] << ", (*startSector)[2]: " << (*startSector)[2] << "\n";
        std::cout << "*endSector[0]: " << (*endSector)[0] << ", (*endSector)[1]: " << (*endSector)[1] << ", (*endSector)[2]: " << (*endSector)[2] << "\n";

        cv::Mat newSector = getNewSector(*startSector, *endSector, 1.0, startImage, count++);

        std::cout << "newSector" << newSector.size() << "\n";
        morphed |= newSector;

        std::cout << "Merged with image\n";

        cv::imwrite(boost::lexical_cast<std::string>(count).append(".jpg"), newSector);

        std::cout << "Saved as " << count << ".jpg\n";

        ++startSector;
        ++endSector;
    }

    std::cout << "Saving newSector.jpg .. "; 
    cv::imwrite("newSector.jpg", morphed);
    std::cout << "Saved\n";
    endImage = morphed;
    sleep(100);
    return;
}
