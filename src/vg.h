#ifndef VG_H_
#define VG_H_

#include <string>
#include <vector>
#include <exception>
#include <algorithm>
#include <boost/thread/thread.hpp>
#include <boost/lexical_cast.hpp>
#include "opencv2/core/core.hpp"
#include "opencv2/objdetect/objdetect.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "image.h"
#include <redispp.h>

typedef std::vector<cv::Point2f *> Sector;

class VGenerator
{

    std::vector<Image*> images;
    FILE* pipe;
    redispp::Connection* conn;
    std::string videoKey;

    int fps;
    int transition_frames;
    int wait_frames;

    bool initVideoPipe(std::string videoLoc, std::string ffmpeg_location);
    bool morph(cv::Mat img1, cv::Mat img2, cv::Mat &lastFrame);
    bool morph(cv::Mat img1, cv::Mat img2, cv::Mat &lastFrame, int transition_frames, int wait_frames, bool toblack);
    bool frameToVideo(cv::Mat frame);
    bool bytesToVideo(char* frameBytes, int size);
    void realMorph(cv::Mat startImage, std::vector<Sector> startSectors, cv::Mat endImage, std::vector<Sector> endSectors, cv::Mat &lastFrame);

    public:
        VGenerator(std::vector<Image*> images, int fps, int transition_frames, int wait_frames, std::string videoKey, redispp::Connection* conn)
        {
            this->images = images;
            this->fps = fps;
            this->transition_frames = transition_frames;
            this->wait_frames = wait_frames;
            this->conn = conn;
            this->videoKey = videoKey;
        };
        ~VGenerator() {};
        void createVideo(std::string videoLoc);
};

#endif // VG_H_