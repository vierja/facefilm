import facebook
import os, sys
from datetime import datetime
import json
import urllib
import workerpool
import random
import json


class FacebookDownloader(object):
    """docstring for FacebookDownloader"""
    def __init__(self, graph, base_dir, save_file):
        super(FacebookDownloader, self).__init__()
        self.graph = graph
        self.base_dir = base_dir
        self.save_file = save_file

    def get_photo(self, tag):
        tag_x = tag['xcoord']
        tag_y = tag['ycoord']
        photo_data = self.graph.get_object(str(tag['object_id']))
        source_url = photo_data['images'][0]['source']
        image_name = self.base_dir + datetime.strptime(photo_data['created_time'],'%Y-%m-%dT%H:%M:%S+0000').strftime('%Y%m%d%H%M%S') + str(photo_data["id"][:10]) + ".jpg"
        process_image(source_url, image_name, photo_data['created_time'], tag_x, tag_y, len(photo_data['tags']['data']), save_file)

def process_image(url, local_file, photo_date, tag_x, tag_y, num_tags, save_file):
    f = open(local_file, 'wb')
    f.write(urllib.urlopen(url).read())
    f.close()
    res_line = local_file + "," + str(tag_x)[:5] + "," + str(tag_y)[:5] + "," + str(num_tags)
    print res_line
    if save_file:
        save_file.write(res_line + '\n')

def get_all_tags(access_token, user_id, user_base_dir, num_threads, save_file):
    user_id = str(user_id)
    graph = facebook.GraphAPI(access_token)
    tags = graph.fql("select object_id, xcoord, ycoord from photo_tag where subject = " + str(user_id))
    print "Found", len(tags), "tags for", user_id

    #WTF Tengo que llamar a esto para que no explote time en los threads.
    datetime.strptime("20100101","%Y%m%d")

    fb = FacebookDownloader(graph, user_base_dir, save_file)
    pool = workerpool.WorkerPool(size=num_threads)
    pool.map(fb.get_photo, tags)
    pool.shutdown()
    pool.wait()

if __name__ == "__main__":

    user_id = int(sys.argv[1])

    if len(sys.argv) > 2:
        save_file_name = sys.argv[2]
    else:
        save_file_name = None


    # Load config files
    config_data = open('config.json')
    config = json.load(config_data)

    # Set num threads for downloading
    num_threads = 30
    if "threads" in config:
        num_threads = int(config["threads"])

    # Set base dir for downloading images.
    base_dir = config["base_dir"]
    if base_dir[-1] != '/':
        base_dir += '/'

    assert os.path.isdir(base_dir)

    user_base_dir = base_dir + str(user_id) + '/'
    if not os.path.isdir(user_base_dir):
        os.makedirs(user_base_dir)

    save_file = None
    if save_file_name:
        # Open file for saving.
        save_file = open(save_file_name, 'wb')

    print "Saving all", user_id, "tagged images in dir", user_base_dir, "with", num_threads, "threads."
    if save_file:
        print "Saving output to", save_file_name

    get_all_tags(config["access_token"], user_id, user_base_dir, num_threads, save_file)

    if save_file:
        save_file.close()

