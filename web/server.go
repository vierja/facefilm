package main

import (
	"encoding/json"
	"flag"
	"github.com/garyburd/redigo/redis"
	"github.com/go-martini/martini"
	gooauth2 "github.com/golang/oauth2"
	"github.com/gorilla/websocket"
	fb "github.com/huandu/facebook"
	"github.com/martini-contrib/binding"
	"github.com/martini-contrib/oauth2"
	"github.com/martini-contrib/render"
	"github.com/martini-contrib/sessions"
	"github.com/nutrun/lentil"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"
)

var letters = []rune("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789")

var (
	redisAddress      = flag.String("redis", "127.0.0.1:6379", "Address to the Redis server")
	maxConnections    = flag.Int("max-redis-conn", 10, "Max connections to Redis")
	beanstalkdAddress = flag.String("beanstalkd", "127.0.0.1:11300", "Address to the Beanstalkd server")
	beanstalkdTube    = flag.String("facetube", "facefilm_jobs", "Tube used by beanstlakd")
	uploadTube        = flag.String("uploadtube", "facefilm_uploads", "Tube used by beanstlakd for uploading videos")
	createVideoTube   = flag.String("createvideotube", "facefilm_create_video", "Tube used by beanstalkd for creating videos")
	adminUsers        = flag.String("adminusers", "1244910023", "admin users to create unlimited videos.")
)

var currentTube = *beanstalkdTube

type UploadVideoForm struct {
	VideoName   string `form:"video_name" binding:"required" json:"video_name"`
	AccessToken string `form:"access_token" binding:"required" json:"access_token"`
	VideoId     string `form:"video_id" json:"video_id"`
}

type CreateVideoForm struct {
	Photos           []int64 `form:"photos[]" binding:"required" json:"photos"`
	TransitionFrames int     `form:"transition_frames" json:"transition_frames"`
}

type VideoRedis struct {
	UserID          string  `redis:"user_id" json:"user_id"`
	VideoId         string  `redis:"video_id" json:"video_id"`
	Status          string  `redis:"status" json:"status"` // Possible status pending, in_process, done_faces, pending_video, in_process_video, done, error_faces, error_video
	FDProgress      float64 `redis:"fd_progress" json:"fd_progress"`
	VGProgress      float64 `redis:"vg_progress" json:"vg_progress"`
	FoundFace       float64 `redis:"found_face" json:"found_face"`
	NotFoundFace    float64 `redis:"not_found_face" json:"not_found_face"`
	TotalImages     float64 `redis:"total_images" json:"total_images"`
	VideoName       string  `redis:"video_name" json:"video_name"`
	FacebookVideoId string  `redis:"facebook_video_id" json:"facebook_video_id"`
}

func randSeq(n int) string {
	b := make([]rune, n)
	for i := range b {
		b[i] = letters[rand.Intn(len(letters))]
	}
	return string(b)
}

func videoKey(id string) string {
	return "video:" + id
}

func userKey(id string) string {
	return "user:" + id
}

func isAdmin_(id string, admins string) bool {
	admin_list := strings.Split(admins, ",")
	for _, admin_id := range admin_list {
		if admin_id == id {
			return true
		}
	}
	return false
}

func main() {
	m := martini.Classic()

	flag.Parse()
	rand.Seed(time.Now().UTC().UnixNano())

	redisConn := redis.NewPool(func() (redis.Conn, error) {
		c, err := redis.Dial("tcp", *redisAddress)

		if err != nil {
			log.Println("Error connecting to redis.")
			return nil, err
		}

		return c, err
	}, *maxConnections)

	beansConn, err := lentil.Dial(*beanstalkdAddress)

	if err != nil {
		log.Println("Error connecting to beanstalkd.")
		return
	}

	beansConn.Use(*beanstalkdTube)

	defer redisConn.Close()
	defer beansConn.Quit()

	m.Map(redisConn)
	m.Map(beansConn)

	// Static service
	m.Use(martini.Static("static"))

	// Session service
	store := sessions.NewCookieStore([]byte("asdajioaisdjoiajsdoiw318923u12893uwdaisjo"))
	m.Use(sessions.Sessions("oh_hi_mark", store))

	// OAuth service. oauth2callback is a oauth2 defined URL.
	m.Use(oauth2.Facebook(&gooauth2.Options{
		ClientID:     "",
		ClientSecret: "",
		RedirectURL:  "http://www.facefilm.me/oauth2callback",
		Scopes:       []string{"public_profile", "user_photos"},
	}))

	// Rendering service with templates with inheritance.
	m.Use(render.Renderer(render.Options{
		Directory: "templates",
		Layout:    "base",
	}))

	/*
		Urls:

		/ - home
		/video/(?P<videoId>[a-zA-Z0-9]+)/faces.sock - face detection socket endpoint
		/login2 - save facebook credentials to session
		/video/new - start new video
		/video/(?P<videoId>[a-zA-Z0-9]+) - get video html page
		/video/(?P<videoId>[a-zA-Z0-9]+)/create_video - generate video video
		/video/(?P<videoId>[a-zA-Z0-9]+)/faces.json - list of detected faces of video
		/video/(?P<videoId>[a-zA-Z0-9]+)/(?P<videoName>[a-zA-Z0-9]+).json - permission endpoint to verify video file ownership
		/video/(?P<videoId>[a-zA-Z0-9]+).json - video details api
		/video/(?P<videoId>[a-zA-Z0-9]+)/upload - upload video to facebook

	*/

	m.Get("/", func(redisConn *redis.Pool, tokens oauth2.Tokens, s sessions.Session, r render.Render) {

		loggedIn := !tokens.IsExpired()

		var videos []string

		isAdmin := false

		if loggedIn {
			c := redisConn.Get()
			defer c.Close()
			videos, _ = redis.Strings(c.Do("LRANGE", "user:"+s.Get("id").(string), 0, -1))
			isAdmin = isAdmin_(s.Get("id").(string), *adminUsers)
		}
		obj := map[string]interface{}{"title": "Facefilm - Yet Another Facefilm App", "logged_in": loggedIn, "videos": videos, "is_admin": isAdmin}
		r.HTML(200, "index", obj)
	})

	m.Get("/me.json",
		oauth2.LoginRequired,
		func(redisConn *redis.Pool, t oauth2.Tokens, s sessions.Session, r render.Render, p martini.Params) {

			c := redisConn.Get()
			defer c.Close()

			videos, _ := redis.Strings(c.Do("LRANGE", "user:"+s.Get("id").(string), 0, -1))

			var jsonVideos = make([]VideoRedis, len(videos))

			for i, video := range videos {

				var videoStruct VideoRedis
				values, _ := redis.Values(c.Do("HGETALL", "video:"+video))
				if err := redis.ScanStruct(values, &videoStruct); err != nil {
					r.JSON(403, map[string]interface{}{"error": "Invalid video in user list."})
					return
				}

				jsonVideos[i] = videoStruct
			}

			r.JSON(200, map[string]interface{}{"user_id": s.Get("id"), "name": s.Get("name"), "videos": jsonVideos})
		})

	m.Get("/video/(?P<videoId>[a-zA-Z0-9]+)/faces.sock", func(w http.ResponseWriter, r *http.Request, redisConn *redis.Pool, p martini.Params) {
		c := redisConn.Get()
		defer c.Close()

		videoKey := videoKey(p["videoId"])
		values, err := redis.Values(c.Do("HGETALL", videoKey))
		var video VideoRedis
		if err := redis.ScanStruct(values, &video); err != nil {
			log.Println("Invalid user for socket. Video does not belong to user.")
			http.Error(w, "Invalid user for socket", 400)
			return
		}

		ws, err := websocket.Upgrade(w, r, nil, 1024, 1024)
		if _, ok := err.(websocket.HandshakeError); ok {
			log.Println("Error upgrade the socket in handshake.", err)
			http.Error(w, "Not a websocket handshake", 400)
			return
		} else if err != nil {
			log.Println(err)
			return
		}

		defer ws.Close()

		psc := redis.PubSubConn{c}
		psc.Subscribe(videoKey + ":faces_channel")
		for {
			switch v := psc.Receive().(type) {
			case redis.Message:
				ws.WriteMessage(1, []byte(v.Data))
			case redis.Subscription:
				log.Printf("%s: %s %d\n", v.Channel, v.Kind, v.Count)
			case error:
				log.Println("Error", v)
			}
		}

		defer log.Println("Closed socket connection.")

	})

	m.Get("/login2", oauth2.LoginRequired, func(t oauth2.Tokens, s sessions.Session, r render.Render) {

		res, _ := fb.Get("/me", fb.Params{
			"access_token": t.Access(),
		})

		s.Set("id", res["id"])
		s.Set("name", res["name"])

		r.Redirect("/")
	})

	m.Post("/video/new",
		oauth2.LoginRequired,
		func(redisConn *redis.Pool, beansConn *lentil.Beanstalkd, t oauth2.Tokens, s sessions.Session, r render.Render) {
			videoId := randSeq(10)
			videoName := randSeq(25) + ".mp4"
			videoKey := videoKey(videoId)

			c := redisConn.Get()
			defer c.Close()

			numberVideos, err := redis.Int(c.Do("LLEN", userKey(s.Get("id").(string))))

			if numberVideos > 0 && !isAdmin_(s.Get("id").(string), *adminUsers) {
				r.JSON(403, map[string]interface{}{"error": "You have already created a video."})
				return
			}

			// Start redis pipeline
			c.Send("MULTI")
			// Save userId, access token and name in redis under key: videoId.
			c.Send("HSET", videoKey, "user_id", s.Get("id").(string))
			c.Send("HSET", videoKey, "access_token", t.Access())
			c.Send("HSET", videoKey, "name", s.Get("name").(string))
			c.Send("HSET", videoKey, "video_name", videoName)
			c.Send("HSET", videoKey, "status", "pending")
			c.Send("HSET", videoKey, "fd_progress", 0)
			c.Send("HSET", videoKey, "vg_progress", 0)
			// Save the videoId in a list of userakeId videos
			c.Send("LPUSH", userKey(s.Get("id").(string)), videoId)

			_, err = c.Do("EXEC")
			if err != nil {
				r.Redirect("/video/error/redis")
				return
			}

			if currentTube != *beanstalkdTube {
				currentTube = *beanstalkdTube
				beansConn.Use(*beanstalkdTube)
			}

			_, err = beansConn.Put(0, 0, 0, []byte(videoId))
			if err != nil {
				r.Redirect("/video/error/beanstalkd")
				return
			}

			r.Redirect("/video/" + videoId)
		})

	m.Get("/video/(?P<videoId>[a-zA-Z0-9]+)",
		oauth2.LoginRequired,
		func(redisConn *redis.Pool, beansConn *lentil.Beanstalkd, t oauth2.Tokens, s sessions.Session, r render.Render, p martini.Params) {
			obj := map[string]interface{}{"title": s.Get("name").(string) + "'s video", "video_id": p["videoId"], "logged_in": true}

			c := redisConn.Get()
			defer c.Close()

			redisUserId, err := redis.String(c.Do("HGET", videoKey(p["videoId"]), "user_id"))
			if err != nil || redisUserId != s.Get("id").(string) {
				r.JSON(403, map[string]interface{}{"error": "Invalid video id"})
				return
			}

			r.HTML(200, "video", obj)
		})

	m.Post("/video/(?P<videoId>[a-zA-Z0-9]+)/create_video",
		binding.Bind(CreateVideoForm{}),
		oauth2.LoginRequired,
		func(createVideo CreateVideoForm, redisConn *redis.Pool, beansConn *lentil.Beanstalkd, t oauth2.Tokens, s sessions.Session, r render.Render, p martini.Params) {

			c := redisConn.Get()
			defer c.Close()

			videoKey := videoKey(p["videoId"])

			redisUserId, err := redis.String(c.Do("HGET", videoKey, "user_id"))
			if err != nil || redisUserId != s.Get("id").(string) {
				r.JSON(403, map[string]interface{}{"error": "Invalid video id"})
				return
			}

			if currentTube != *createVideoTube {
				currentTube = *createVideoTube
				beansConn.Use(*createVideoTube)
			}

			if createVideo.TransitionFrames != 0 {
				if createVideo.TransitionFrames < 3 || createVideo.TransitionFrames > 18 {
					r.JSON(403, map[string]interface{}{"error": "invalid value for transition frames"})
				}
			}

			// Fuck errors
			videoPhotos, _ := json.Marshal(createVideo)

			c.Send("MULTI")
			// Save userId, access token and name in redis under key: videoId.
			c.Send("HSET", videoKey, "allowed_photos", string(videoPhotos))
			c.Send("HSET", videoKey, "status", "pending_video")

			if createVideo.TransitionFrames > 0 {
				c.Send("HSET", videoKey, "transition_frames", createVideo.TransitionFrames)
			}

			_, err = c.Do("EXEC")
			if err != nil {
				r.Redirect("/video/error/redis")
				return
			}

			_, err = beansConn.Put(0, 0, 0, []byte(p["videoId"]))
			if err != nil {
				r.Redirect("/video/error/beanstalkd")
				return
			}

			r.JSON(201, map[string]interface{}{"status": "ok"})
		})

	m.Get(
		"/video/(?P<videoId>[a-zA-Z0-9]+)/faces.json",
		oauth2.LoginRequired,
		func(redisConn *redis.Pool, beansConn *lentil.Beanstalkd, t oauth2.Tokens, s sessions.Session, r render.Render, p martini.Params) {
			c := redisConn.Get()
			defer c.Close()

			videoKey := videoKey(p["videoId"])

			redisUserId, err := redis.String(c.Do("HGET", videoKey, "user_id"))

			if err != nil || redisUserId != s.Get("id").(string) {
				r.JSON(403, map[string]interface{}{"error": "Invalid video ida"})
				return
			}

			faces, _ := redis.Strings(c.Do("LRANGE", videoKey+":faces", 0, -1))

			var jsonFaces = make([]map[string]interface{}, len(faces))

			for i, face := range faces {
				var dat map[string]interface{}
				if err := json.Unmarshal([]byte(face), &dat); err != nil {
					r.JSON(500, map[string]interface{}{"error": "With redis json faces"})
					return
				}
				jsonFaces[len(faces)-1-i] = dat
			}

			r.JSON(200, jsonFaces)
		})

	m.Get("/video/(?P<videoId>[a-zA-Z0-9]+)/(?P<videoName>[a-zA-Z0-9]+).json",
		func(tokens oauth2.Tokens, redisConn *redis.Pool, s sessions.Session, r render.Render, p martini.Params) {
			if tokens.IsExpired() {
				r.JSON(403, map[string]interface{}{"error": "Hacka"})
				return
			}

			c := redisConn.Get()
			defer c.Close()

			values, err := redis.Values(c.Do("HGETALL", videoKey))

			var video VideoRedis
			if err := redis.ScanStruct(values, &video); err != nil {
				r.JSON(403, map[string]interface{}{"error": "Invalid video id assz"})
				return
			}

			if err != nil || video.UserID != s.Get("id").(string) {
				r.JSON(403, map[string]interface{}{"error": "Hacka mata"})
				return
			}

			if err != nil || video.VideoName != p["videoName"]+".mp4" {
				r.JSON(403, map[string]interface{}{"error": "smart Hackas"})
				return
			}

			r.JSON(200, map[string]interface{}{"error": "good to go"})
		})

	m.Get(
		"/video/(?P<videoId>[a-zA-Z0-9]+).json",
		oauth2.LoginRequired,
		func(redisConn *redis.Pool, beansConn *lentil.Beanstalkd, t oauth2.Tokens, s sessions.Session, r render.Render, p martini.Params) {
			c := redisConn.Get()
			defer c.Close()

			videoKey := videoKey(p["videoId"])

			values, err := redis.Values(c.Do("HGETALL", videoKey))

			var video VideoRedis
			if err := redis.ScanStruct(values, &video); err != nil {
				r.JSON(403, map[string]interface{}{"error": "Invalid video id assz"})
				return
			}

			if err != nil || video.UserID != s.Get("id").(string) {
				r.JSON(403, map[string]interface{}{"error": "Invalid video id"})
				return
			}

			r.JSON(200, map[string]interface{}{
				"user_id":           video.UserID,
				"video_id":          video.VideoId,
				"status":            video.Status,
				"fd_progress":       video.FDProgress,
				"vg_progress":       video.VGProgress,
				"found_face":        video.FoundFace,
				"not_found_face":    video.NotFoundFace,
				"total_images":      video.TotalImages,
				"video_name":        video.VideoName,
				"facebook_video_id": video.FacebookVideoId,
			})
		})

	m.Post(
		"/video/(?P<videoId>[a-zA-Z0-9]+)/upload",
		oauth2.LoginRequired,
		binding.Bind(UploadVideoForm{}),
		func(uploadVideo UploadVideoForm, redisConn *redis.Pool, beansConn *lentil.Beanstalkd, t oauth2.Tokens, s sessions.Session, r render.Render, p martini.Params) {

			c := redisConn.Get()
			defer c.Close()

			videoKey := videoKey(p["videoId"])

			values, err := redis.Values(c.Do("HGETALL", videoKey))

			var video VideoRedis
			if err := redis.ScanStruct(values, &video); err != nil {
				r.JSON(403, map[string]interface{}{"error": "Invalid video id assz"})
				return
			}

			if err != nil || video.UserID != s.Get("id").(string) {
				r.JSON(403, map[string]interface{}{"error": "Invalid video id"})
				return
			}

			if err != nil || video.VideoName != uploadVideo.VideoName {
				r.JSON(403, map[string]interface{}{"error": "becauuse??"})
			}

			if video.FacebookVideoId != "" {
				r.JSON(200, map[string]interface{}{"facebook_video_id": video.FacebookVideoId, "status": "found"})
				return
			}

			if currentTube != *uploadTube {
				currentTube = *uploadTube
				beansConn.Use(*uploadTube)
			}

			uploadVideo.VideoId = p["videoId"]

			// Fuck errors
			uploadVideoJson, _ := json.Marshal(uploadVideo)

			var jsonStr = []byte(uploadVideoJson)

			_, err = beansConn.Put(0, 0, 0, jsonStr)
			if err != nil {
				r.Redirect("/video/error/beanstalkd")
				return
			}

			r.JSON(200, map[string]interface{}{"status": "pending"})

		})

	m.Run()
}
