/** @jsx React.DOM */

/*global React, Backbone */
var app = app || {};

(function () {
    'use strict';

    var FacebookUpload = React.createClass({displayName: 'FacebookUpload',
        getInitialState: function() {
            return {
                facebook_video_id: '',
                uploading_video: false
            };
        },

        askFacebookPermission: function() {
            FB.login(function(response) {
                if (response.authResponse && response.authResponse.accessToken) {
                    this.newAccessToken = response.authResponse.accessToken;
                    this.uploadVideoToFacebook();
                }
            }.bind(this), {scope: 'publish_actions'});
        },

        uploadVideoToFacebook: function() {
            $.post("/video/" + this.props.videoId + "/upload",
                { video_name: this.props.video_name, access_token: this.newAccessToken },
                function(data) {
                    this.intervalFacebookVideoId = setInterval(this.getFacebookVideoId, 1000);
                    this.setState({uploading_video: true})
                }.bind(this)
            );
        },

        getFacebookVideoId: function() {
            if (!this.updatingProgress) {
                this.updatingProgress = true;
                $.ajax({
                    url: this.props.url,
                    dataType: 'json',
                    success: function(data) {
                        if (data.facebook_video_id) {
                            this.props.facebook_video_id = data.facebook_video_id;
                            this.setState({uploading_video: false})
                            clearInterval(this.intervalFacebookVideoId);
                            this.intervalFacebookVideoId = false;
                        }
                    }.bind(this)
                });
                this.updatingProgress = false;
            }
        },

        renderSpinner: function() {
            return (
                React.DOM.a({className: "btn btn-block btn-social btn-sm btn-facebook", href: "javascript:void(0)", style: {'height': '33px', 'width': '170px'}}, 
                React.DOM.i({className: "fa fa-facebook", style: {'margin-top': '2px'}}), 
                React.DOM.div({className: "spinner"}, 
                    React.DOM.i({className: "shithole"}), 
                    React.DOM.div({className: "bounce1"}), 
                    React.DOM.div({className: "bounce2"}), 
                    React.DOM.div({className: "bounce3"})
                )
                )
            );
        },

        render: function() {
            var share;
            if (this.state.uploading_video) {
                share = this.renderSpinner();
            } else if (this.props.facebook_video_id.length > 0) {
                share = React.DOM.a({className: "btn btn-block btn-social btn-sm btn-facebook", style: {'height': '33px', 'width': '170px'}, href: 'https://www.facebook.com/video.php?v=' + this.props.facebook_video_id, target: "_blank"}, React.DOM.i({className: "fa fa-facebook", style: {'margin-top': '2px'}}), "View on Facebook");
            } else {
                share = React.DOM.a({className: "btn btn-block btn-social btn-sm btn-facebook", style: {'height': '33px', 'width': '170px'}, href: "javascript:void(0)", onClick: this.askFacebookPermission}, React.DOM.i({className: "fa fa-facebook", style: {'margin-top': '2px'}}), "Upload to facebook");
            }

            return  React.DOM.div({className: "pull-right"}, 
                        share
                    );
        }
    });

    var ProgressBar = React.createClass({displayName: 'ProgressBar',
        getInitialState: function() {
            return {
                loaded_data: false,
                status: 'pending',
                fd_progress: 0,
                vg_progress: 0,
                video_name: '',
                permission_to_upload: false,
                facebook_video_id: '',
                uploading_video: false,
                done: false
            };
        },
        updateProgress: function() {
            // Avoid parallels update with local lock
            if (!this.updatingProgress) {
                this.updatingProgress = true;

                $.ajax({
                    url: this.props.url,
                    dataType: 'json',
                    success: function(data) {
                        var fd_progress = 0, vg_progress = 0;
                        if (data.total_images > 0) {
                            fd_progress = (data.fd_progress/data.total_images * 100).toFixed(1);
                        }
                        if (data.total_images > 0) {
                            vg_progress = (data.vg_progress/data.total_images * 100).toFixed(1);
                        }

                        /*
                        Update parent status.
                        */
                        if (vg_progress > 0 && this.state.vg_progress == 0) {
                            // Notify parent of first status that vg progress is detected.
                            this.props.vgStarted();
                        }

                        if (fd_progress >= 100 && this.state.fd_progress != fd_progress) {
                            // Notify parent of finished face detection.
                            this.props.fdDone();
                        }

                        if (fd_progress > 0 && this.state.fd_progress == 0) {
                            // Notify parent of first status that fd progress is detected.
                            this.props.fdStarted();
                        }

                        if (data.status == 'done_faces') {

                            clearInterval(this.interval);
                            this.interval = false;
                            this.setState({
                                fd_progress: fd_progress,
                                vg_progress: vg_progress,
                                loaded_data: true,
                                status: data.status
                            });

                        } else if (data.status == 'done') {

                            clearInterval(this.interval);
                            this.interval = false;
                            this.setState({
                                fd_progress: fd_progress,
                                vg_progress: vg_progress,
                                video_name: data.video_name,
                                status: data.status,
                                facebook_video_id: data.facebook_video_id,
                                loaded_data: true,
                                done: true
                            });

                        } else if (data.status == 'error') {
                            clearInterval(this.interval);
                            this.interval = false;
                            this.setState({loaded_data: true, status: data.status});

                        } else {
                            this.setState({
                                fd_progress: fd_progress,
                                vg_progress: vg_progress,
                                loaded_data: true,
                                status: data.status
                            });
                        }

                    }.bind(this),
                    error: function(xhr, status, err) {
                        console.error(this.props.url, status, err.toString());
                    }.bind(this)
                });

                this.updatingProgress = false;
            }
        },

        componentDidMount: function() {
            this.interval = setInterval(this.updateProgress, 1000);
        },

        componentDidUpdate: function () {
            if (this.props.vg_started && this.interval === false && this.state.done == false) {
                console.log("Setting interval for updates");
                this.interval = setInterval(this.updateProgress, 1000);
            }
        },

        renderShareBar: function() {

            return (
                React.DOM.div({className: "row actions-row"}, 
                    React.DOM.div({className: "col-xs-6"}, 
                       React.DOM.a({href: '/video/' + app.videoId + '/' + this.state.video_name, download: "MyFacefilm.mp4", target: "_blank", className: "btn btn-default btn-sm"}, "Download video")
                    ), 
                    React.DOM.div({className: "col-xs-6"}, 
                        FacebookUpload({
                            videoId: app.videoId, 
                            video_name: this.state.video_name, 
                            url: this.props.url, 
                            facebook_video_id: this.state.facebook_video_id}
                        )
                    )
                )
            );
        },

        render: function() {

            if (!this.state.loaded_data) {
                return React.DOM.div(null);
            }

            var link_to_video;
            var embedded_video;
            var cx = React.addons.classSet;

            var fd_progress_classes = "progress progress-striped active";
            var vg_progress_classes = "progress progress-striped active";

            if (this.state.fd_progress >= 100) {
                fd_progress_classes = "progress";
            }

            if (this.state.status == 'done') {
                return (
                    React.DOM.div(null, 
                        this.renderShareBar(), 
                        React.DOM.div({className: "videocontent video_div"}, 
                            React.DOM.video({id: "face_video_player", className: "video-js vjs-default-skin", controls: "controls", preload: "auto", width: "auto", height: "auto", 
                                'data-setup': "{}"}, 
                                React.DOM.source({src: '/video/' + this.props.videoId + '/' + this.state.video_name, type: "video/mp4"}), 
                                React.DOM.p({className: "vjs-no-js"}, "To view this video please enable JavaScript, and consider upgrading to a web browser that ", React.DOM.a({href: "http://videojs.com/html5-video-support/", target: "_blank"}, "supports HTML5 video"))
                            )
                        )
                    )
                );

            } else if (this.state.status == 'in_process_video') {
                return (
                    React.DOM.div(null, 
                        "Video generation.", 
                        React.DOM.div({className: vg_progress_classes}, 
                            React.DOM.div({className: "progress-bar", role: "progressbar", 'aria-valuenow': "2", 'aria-valuemin': "0", 'aria-valuemax': "100", style: {"width": this.state.vg_progress + "%"}}, 
                                this.state.vg_progress, " %"
                            )
                        ), 
                        embedded_video
                    )
                );
            } else if (this.state.status == 'in_process' || this.state.status == 'done_faces') {
                return (
                    React.DOM.div(null, 
                        "Face detection progress.", 
                        React.DOM.div({className: fd_progress_classes}, 
                            React.DOM.div({className: "progress-bar", role: "progressbar", 'aria-valuenow': "2", 'aria-valuemin': "0", 'aria-valuemax': "100", style: {"width": this.state.fd_progress + "%"}}, 
                                this.state.fd_progress, " %"
                            )
                        )
                    )
                );
            } else if (this.state.status == 'pending' || this.state.status == 'pending_video') {
                return (
                    React.DOM.div(null, 
                        "Waiting for resources to free ...",  
                        React.DOM.div({className: "resources-spinner"})
                    )
                );
            } else if (this.state.status == 'error') {
                return (
                    React.DOM.div(null, 
                        "Oops, there seemed to be an error during the process. Please try to create the video again. (sorry)"
                    )
                );
            }

        }
    });

    var PhotoSelector = React.createClass({displayName: 'PhotoSelector',

        getInitialState: function () {
            return {use_this: true}
        },

        changeUseThis: function () {

            var new_value = !this.state.use_this;
            this.setState({use_this: new_value});

            if (new_value) {
                this.props.removeIgnoredPhoto(this.props.photo.object_id);
            } else {
                this.props.addIgnoredPhoto(this.props.photo.object_id);
            }
        },

        render: function () {

            // in px except specified otherwise
            var width = 200, height = 200;

            var image_styles = {
                'height': height + 'px',
                'width': width + 'px',
                'background-image': 'url(' + this.props.photo.src +')',
                'background-repeat': 'no-repeat',
                'background-color': 'rgb(180, 205, 200)'
            }

            var classNames = "facebox";
            var usethis;

            if (this.props.photo.status == 'found') {

                var ideal_face_width = 200;

                var ratio_to_ideal = ideal_face_width / this.props.photo.face_width;

                var left_right_margins = (width - this.props.photo.face_width * ratio_to_ideal) / 2;
                var top_bottom_margins = (height - this.props.photo.face_height * ratio_to_ideal) / 2;

                var position_x = -this.props.photo.face_x * ratio_to_ideal + left_right_margins;
                var position_y = -this.props.photo.face_y * ratio_to_ideal + top_bottom_margins;

                image_styles['background-position'] = position_x + 'px ' + position_y + 'px';
                image_styles['background-size'] = this.props.photo.width * ratio_to_ideal + 'px ' + this.props.photo.height * ratio_to_ideal + 'px ';

                if (!this.state.use_this) {
                    image_styles['opacity'] = '0.5';
                }

                classNames += ' fb-image';

                usethis = (
                    React.DOM.div({className: "usethis"}, 
                        React.DOM.label({style: {'width': '100%'}}, 
                            React.DOM.input({type: "checkbox", checked: this.state.use_this, onChange: this.changeUseThis}), 
                            React.DOM.span({style: {'font-size': '13px', 'margin-left': '5px'}}, "Use this")
                        )
                    )
                );

            } else {
                image_styles['background-size'] = '100% auto';
            }

            return (
                React.DOM.div({className: "col-sm-3 face-square"}, 
                    React.DOM.div({style: image_styles, className: classNames}, 
                        usethis
                    )
                )
            );


        }
    });

    var PhotoFilter = React.createClass({displayName: 'PhotoFilter',

        getInitialState: function () {
            return {
                photos: [],
                show_not_found: false,
                show_advanced_settings: false,
                transition_frames: '12'
            }
        },

        addIgnoredPhoto: function (photo_id) {
            if (_.indexOf(this.ignored_photos, photo_id) == -1) {
                this.ignored_photos.push(photo_id);
            }
        },

        removeIgnoredPhoto: function (photo_id) {
            this.ignored_photos = _.without(this.ignored_photos, photo_id);
        },

        processMessage: function (message) {
            if (_.isString(message)) {
                message = JSON.parse(message);
            }

            var existing = _.findWhere(this.state.photos, {object_id: message.object_id});

            if (existing == undefined) {
                var next_photos = this.state.photos.concat([message]);
                this.setState({photos: next_photos});

                if (message.tagged_friends) {
                    _.each(message.tagged_friends, function (tag) {
                        if (tag.user_id) {
                            this.tagged_friends.push(tag);
                            this.friends_names[tag.user_id] = tag.name;
                        }
                    }.bind(this));
                }
            }
        },

        pullAlreadyRequestedImages: function () {
            $.ajax({
                url: this.props.facesUrl,
                dataType: 'json',
                success: function(data) {
                    _.each(data, function(image) {
                        this.processMessage(image);
                    }.bind(this));
                }.bind(this)
            });
        },

        requestDetectedImages: function () {

            if (!this.props.fd_done) {
                console.log("Creating socket.");
                this.socket = new WebSocket(this.props.socketUrl);
                this.socket.onmessage = function (event) {
                    this.processMessage(event.data);
                }.bind(this);
            } else {
                console.log("Doesn't create websocket because fd is done.");
            }

            this.pullAlreadyRequestedImages();
        },

        componentWillMount: function () {
            this.requestDetectedImages();
            console.log("Settings this.ignored_photos.");
            this.ignored_photos = [];
            this.tagged_friends = [];
            this.ignore_friends = [];
            this.friends_names = {};
        },

        changeFilter: function () {
            this.setState({show_not_found: !this.state.show_not_found});
        },

        toggleAdvancedSettings: function () {
            this.setState({show_advanced_settings: !this.state.show_advanced_settings});
        },

        speedChange: function (event) {
            this.setState({transition_frames: event.target.value});
        },

        createVideo: function () {
            console.log("Creating video. Generating list of images to use.");

            var photos = _.chain(this.state.photos).filter(function (photo) {

                if (photo.status != 'found') {
                    return false;
                }

                if (_.indexOf(this.ignored_photos, photo.object_id) > -1) {
                    return false;
                }

                return true;
            }.bind(this))
            .map(function (photo) {
                return photo.object_id;
            }).value();

            console.log("Photos:", photos.length, photos);

            $.post("/video/" + this.props.videoId + "/create_video",
                { photos: photos, transition_frames: this.state.transition_frames },
                function(data) {
                    console.log("Sent create_video request.");

                    this.props.vgStarted();
                }.bind(this),
                'json'
            );
        },

        render: function () {
            var photo_rows = [];
            _.each(this.state.photos, function(photo, key) {
                if (photo.status == 'found' || this.state.show_not_found) {
                    photo_rows.push(
                        PhotoSelector({
                            key: photo.object_id, 
                            photo: photo, 
                            addIgnoredPhoto: this.addIgnoredPhoto, 
                            removeIgnoredPhoto: this.removeIgnoredPhoto}
                        )
                    );
                }
            }.bind(this));

            /*
            var friends = _.chain(this.tagged_friends)
                .countBy(function(e) {return e.user_id})
                .pairs()
                .sortBy(function(e) {return -e[1]})
                .value();

            var friends_info = [];

            _.each(friends, function (friend) {

                friends_info.push(<li>{this.friends_names[friend[0]]} ({friend[1]})</li>);

            }.bind(this));*/


            var generate_video_btn;
            var done;
            if (this.props.fd_done) {

                var adv_settings;
                if (!this.state.show_advanced_settings) {
                    adv_settings = (
                        React.DOM.div(null, 
                            React.DOM.br(null), 
                            React.DOM.small(null, 
                                React.DOM.a({href: "javascript:void(0)", onClick: this.toggleAdvancedSettings, style: {'color': '#18bc9c'}}, 
                                    "(Show advanced settings)"
                                )
                            )
                        )
                    );
                } else {
                    adv_settings = (
                        React.DOM.div(null, 
                            React.DOM.br(null), 
                            React.DOM.small(null, 
                                React.DOM.a({href: "javascript:void(0)", onClick: this.toggleAdvancedSettings, style: {'color': '#18bc9c'}}, 
                                    "(Hide advanced settings)"
                                )
                            ), 
                            React.DOM.div({className: "form-group"}, 
                                React.DOM.label({className: "control-label", htmlFor: "transition_frames"}, "Transition frames"), 
                                React.DOM.select({className: "form-control", value: this.state.transition_frames, onChange: this.speedChange}, 
                                    React.DOM.option({value: "3"}, "Extremely fast"), 
                                    React.DOM.option({value: "6"}, "Fast"), 
                                    React.DOM.option({value: "9"}, "Somewhat fast"), 
                                    React.DOM.option({value: "12"}, "Normal (recommended)"), 
                                    React.DOM.option({value: "15"}, "Slow"), 
                                    React.DOM.option({value: "18"}, "Boring slow")
                                )
                            )

                        )
                    );
                }

                generate_video_btn = (
                    React.DOM.div({className: "row", style: {'margin-bottom': '20px'}}, 
                        React.DOM.div({className: "col-sm-6"}, 
                            React.DOM.button({type: "submit", className: "btn btn-default", onClick: this.createVideo}, "Create the video"), 
                            React.DOM.br(null), 
                            adv_settings
                        )
                    )
                );

                done = (
                    React.DOM.div({className: "col-sm-3 face-square"}, 
                        React.DOM.div({style: {'height':'200px', 'width': '200px', 'background-color': 'rgb(180, 205, 200)'}, className: "facebox fb-image"}, 
                            React.DOM.h3({style: {'margin-top': '70px', 'margin-left': '10px'}}, 
                                "That's all!", 
                                React.DOM.br(null), 
                                React.DOM.small({style: {color: '#808080'}}, "Create the video now")
                            )
                        )
                    )
                );
            }

            return (
                React.DOM.div(null, 
                    generate_video_btn, 
                    React.DOM.div({className: "row", style: {'margin-bottom': '20px'}}, 
                        React.DOM.div({className: "col-sm-6"}, 
                            React.DOM.h4(null, "Unselect the images you don’t want in the video.")
                        ), 
                        React.DOM.div({className: "col-sm-6"}, 
                            React.DOM.label({className: "pull-right"}, " Show not found  ", 
                            React.DOM.input({type: "checkbox", checked: this.state.show_not_found, onChange: this.changeFilter})
                            )
                        )
                    ), 
                    React.DOM.div({className: "row"}, 
                        photo_rows, 
                        done
                    )
                )
            );
        }
    })


    var VideoApp = React.createClass({displayName: 'VideoApp',

        getInitialState: function () {
            return {
                fd_started: false,
                fd_done: false,
                vg_started: false,
                vg_done: false
            }
        },

        fdStarted: function () {
            this.setState({fd_started: true});
        },

        fdDone: function () {
            this.setState({fd_done: true});
        },

        vgStarted: function () {
            this.setState({vg_started: true});
        },

        vgDone: function () {
            this.setState({vg_done: true});
        },

        render: function () {

            var photo_filter;
            if (this.state.fd_started && !this.state.vg_started) {
                photo_filter = PhotoFilter({
                    videoId: this.props.videoId, 
                    fd_done: this.state.fd_done, 
                    facesUrl: '/video/' + this.props.videoId + '/faces.json', 
                    socketUrl: 'ws://' + window.location.hostname + '/video/' + this.props.videoId + '/faces.sock', 
                    vgStarted: this.vgStarted}
                );
            }

            return (
                React.DOM.div(null, 
                    ProgressBar({
                        videoId: this.props.videoId, 
                        url: '/video/' + this.props.videoId + '.json', 
                        vg_started: this.state.vg_started, 
                        fdStarted: this.fdStarted, 
                        fdDone: this.fdDone, 
                        vgStarted: this.vgStarted}
                    ), 
                    photo_filter
                )
            );
        }
    })

    React.renderComponent(
        VideoApp({videoId: app.videoId}),
        document.getElementById('progress')
    );
})();