docker run -d -v /opt/redis:/data --name redis dockerfile/redis
docker run -d --name beanstalkd vierja/beanstalkd
docker run -d --name facefilm_worker_1 \
              --link beanstalkd:beanstalkd \
              --link redis:redis \
              -v /opt/videos/:/opt/videos/ \
              -v /var/run/docker.sock:/var/run/docker.sock \
              vierja/facefilm:worker \
              python worker.py \
                    --beanstalkd beanstalkd \
                    --tube facefilm_jobs \
                    --redis redis \
                    --worker-id 1 \
                    --docker unix://var/run/docker.sock \
                    --video-folder /opt/videos
docker run -d --name facefilm_web \
              --link redis:redis \
              --link beanstalkd:beanstalkd \
              -p 3000:3000 \
              vierja/facefilm:web \
              /go/src/github.com/vierja/facefilm-web/facefilm-web \
                    -beanstalkd beanstalkd:11300 \
                    -redis "redis:6379"

make build_web ; docker run -it --rm --name facefilm_web \
              --link redis:redis \
              --link beanstalkd:beanstalkd \
              -p 3000:3000 \
              vierja/facefilm:web \
              /go/src/github.com/vierja/facefilm-web/facefilm-web \
                    -beanstalkd beanstalkd:11300 \
                    -redis "redis:6379"