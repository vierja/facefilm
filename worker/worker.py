# -*- coding: utf-8 -*-
from __future__ import print_function, unicode_literals
import beanstalkc
import redis
import argparse
import docker
import json
import requests
import time
import traceback
import sendgrid

ARGS = argparse.ArgumentParser()
ARGS.add_argument(
    '-b', '--beanstalkd', type=str, default='beanstalkd_facefilm',
    help='Beanstalkd host. (default: "beanstalkd")')
ARGS.add_argument(
    '-t', '--tube', type=str, default='facefilm_jobs',
    help='Facefilm tube. (default: "facefilm_jobs")')
ARGS.add_argument(
    '--save-video-tube', type=str, default='facefilm_uploads',
    help='Facefilm uploads tube. (default: "facefilm_uploads")')
ARGS.add_argument(
    '--create-video-tube', type=str, default='facefilm_create_video',
    help='Facefilm create video tube. (default: "facefilm_create_video")')
ARGS.add_argument(
    '-r', '--redis', type=str, default='redis',
    help='Redis host. (default: "redis")')
ARGS.add_argument(
    '--worker-id', type=int, default=0)
ARGS.add_argument(
    '-d', '--docker', type=str, default='unix://var/run/docker.sock',
    help='Docker socker location. (default: "unix://var/run/docker.sock")')
ARGS.add_argument(
    '--video-folder', type=str, default='/tmp',
    help='Where we should save the videos. (default: "/tmp")')
ARGS.add_argument(
    '--sendgrid_user', type=str, default='javier.rey',
    help='Sendgrid username to send emails (default: "javier.rey")')
ARGS.add_argument(
    '--sendgrid_password', type=str, default='jodermanolo123',
    help='Sendgrid password to send emails (default: "jodermanolo123")')


def create_user_fd_message(video_key, redis, sg):
    try:
        access_token = redis.hget(video_key, 'access_token')
        total_images = redis.hget(video_key, 'total_images')
        found_face = redis.hget(video_key, 'found_face')
        not_found_face = redis.hget(video_key, 'not_found_face')
        video_name = redis.hget(video_key, 'video_name')
        fd_elapsed_time = redis.hget(video_key, 'fd_elapsed_time')
        status = redis.hget(video_key, 'status')

        import facebook
        graph = facebook.GraphAPI(access_token)
        profile = graph.get_object("me")

        message = sendgrid.Mail()
        message.add_to('Javier Rey <javirey@gmail.com>')
        message.set_subject('New user: {0}'.format(profile.get('name', 'No name')))
        message.set_html('''
<html><head><title></title></head><body>
<h2>{name}</h2>

<table border="0" cellpadding="2" cellspacing="2" style="width: 500px;">
    <tbody>
        <tr>
            <td>UserId</td>
            <td style="text-align: center;">{user_id}</td>
        </tr>
        <tr>
            <td>Username</td>
            <td style="text-align: center;">{username}</td>
        </tr>
        <tr>
            <td>Gender</td>
            <td style="text-align: center;">{gender}</td>
        </tr>
        <tr>
            <td>Link</td>
            <td style="text-align: center;">{link}</td>
        </tr>
        <tr>
            <td>Total images</td>
            <td style="text-align: center;">{total_images}</td>
        </tr>
        <tr>
            <td>VideoId</td>
            <td style="text-align: center;">{video_id}</td>
        </tr>
        <tr>
            <td>Found faces</td>
            <td style="text-align: center;">{found_face}</td>
        </tr>
        <tr>
            <td>Not found faces</td>
            <td style="text-align: center;">{not_found_face}</td>
        </tr>
        <tr>
            <td>Video name</td>
            <td style="text-align: center;">{video_name}</td>
        </tr>
        <tr>
            <td>Face detection time</td>
            <td style="text-align: center;">{fd_elapsed_time}</td>
        </tr>
        <tr>
            <td>Status</td>
            <td style="text-align: center;">{status}</td>
        </tr>
    </tbody>
</table>
<p><b>Access token:</b> {access_token}</p></body></html>'''.format(
            name=profile.get('name', 'No name'),
            user_id=profile.get('id', 'No id'),
            username=profile.get('username', 'No username'),
            gender=profile.get('gender', 'No gender'),
            link=profile.get('link', 'No link'),
            total_images=total_images,
            video_id=video_key,
            found_face=found_face,
            not_found_face=not_found_face,
            video_name=video_name,
            fd_elapsed_time=fd_elapsed_time,
            status=status,
            access_token=access_token
        ))
        message.set_from('Javier Rey <javirey@gmail.com>')
        print(sg.send(message))

    except:
        print("Error sending email.")


def create_user_share_message(video_key, redis, sg, facebook_video_id):
    try:
        access_token = redis.hget(video_key, 'access_token')
        total_images = redis.hget(video_key, 'total_images')
        found_face = redis.hget(video_key, 'found_face')
        not_found_face = redis.hget(video_key, 'not_found_face')
        video_name = redis.hget(video_key, 'video_name')
        fd_elapsed_time = redis.hget(video_key, 'fd_elapsed_time')
        transition_frames = redis.hget(video_key, 'transition_frames')
        vg_progress = redis.hget(video_key, 'vg_progress')
        status = redis.hget(video_key, 'status')
        vg_elapsed_time = redis.hget(video_key, 'vg_elapsed_time')

        import facebook
        graph = facebook.GraphAPI(access_token)
        profile = graph.get_object("me")

        message = sendgrid.Mail()
        message.add_to('Javier Rey <javirey@gmail.com>')
        message.set_subject('Video shared: {0}'.format(profile.get('name', 'No name')))
        message.set_html('''
<html><head><title></title></head><body>
<h2>{name}</h2>

<table border="0" cellpadding="2" cellspacing="2" style="width: 500px;">
    <tbody>
        <tr>
            <td>UserId</td>
            <td style="text-align: center;">{user_id}</td>
        </tr>
        <tr>
            <td>Username</td>
            <td style="text-align: center;">{username}</td>
        </tr>
        <tr>
            <td>Facebook Video Id</td>
            <td style="text-align: center;">{facebook_video_id}</td>
        </tr>
        <tr>
            <td>Gender</td>
            <td style="text-align: center;">{gender}</td>
        </tr>
        <tr>
            <td>Link</td>
            <td style="text-align: center;">{link}</td>
        </tr>
        <tr>
            <td>Total images</td>
            <td style="text-align: center;">{total_images}</td>
        </tr>
        <tr>
            <td>VideoId</td>
            <td style="text-align: center;">{video_id}</td>
        </tr>
        <tr>
            <td>Found faces</td>
            <td style="text-align: center;">{found_face}</td>
        </tr>
        <tr>
            <td>Not found faces</td>
            <td style="text-align: center;">{not_found_face}</td>
        </tr>
        <tr>
            <td>Video name</td>
            <td style="text-align: center;">{video_name}</td>
        </tr>
        <tr>
            <td>Face detection time</td>
            <td style="text-align: center;">{fd_elapsed_time}</td>
        </tr>
        <tr>
            <td>Status</td>
            <td style="text-align: center;">{status}</td>
        </tr>
        <tr>
            <td>Video generation time</td>
            <td style="text-align: center;">{vg_elapsed_time}</td>
        </tr>
        <tr>
            <td>Transition frames</td>
            <td style="text-align: center;">{transition_frames}</td>
        </tr>
        <tr>
            <td>Video images</td>
            <td style="text-align: center;">{vg_progress}</td>
        </tr>
    </tbody>
</table>
<p><b>Access token:</b> {access_token}</p></body></html>'''.format(
            name=profile.get('name', 'No name'),
            user_id=profile.get('id', 'No id'),
            facebook_video_id=facebook_video_id,
            username=profile.get('username', 'No username'),
            gender=profile.get('gender', 'No gender'),
            link=profile.get('link', 'No link'),
            total_images=total_images,
            video_id=video_key,
            found_face=found_face,
            not_found_face=not_found_face,
            video_name=video_name,
            fd_elapsed_time=fd_elapsed_time,
            status=status,
            vg_elapsed_time=vg_elapsed_time,
            transition_frames=transition_frames,
            vg_progress=vg_progress,
            access_token=access_token
        ))
        message.set_from('Javier Rey <javirey@gmail.com>')
        print(sg.send(message))

    except:
        print("Error sending email.")


def create_user_vg_message(video_key, redis, sg):
    try:
        access_token = redis.hget(video_key, 'access_token')
        total_images = redis.hget(video_key, 'total_images')
        found_face = redis.hget(video_key, 'found_face')
        not_found_face = redis.hget(video_key, 'not_found_face')
        video_name = redis.hget(video_key, 'video_name')
        fd_elapsed_time = redis.hget(video_key, 'fd_elapsed_time')
        transition_frames = redis.hget(video_key, 'transition_frames')
        vg_progress = redis.hget(video_key, 'vg_progress')
        status = redis.hget(video_key, 'status')
        vg_elapsed_time = redis.hget(video_key, 'vg_elapsed_time')

        import facebook
        graph = facebook.GraphAPI(access_token)
        profile = graph.get_object("me")

        message = sendgrid.Mail()
        message.add_to('Javier Rey <javirey@gmail.com>')
        message.set_subject('Video generated: {0}'.format(profile.get('name', 'No name')))
        message.set_html('''
<html><head><title></title></head><body>
<h2>{name}</h2>

<table border="0" cellpadding="2" cellspacing="2" style="width: 500px;">
    <tbody>
        <tr>
            <td>UserId</td>
            <td style="text-align: center;">{user_id}</td>
        </tr>
        <tr>
            <td>Username</td>
            <td style="text-align: center;">{username}</td>
        </tr>
        <tr>
            <td>Gender</td>
            <td style="text-align: center;">{gender}</td>
        </tr>
        <tr>
            <td>Link</td>
            <td style="text-align: center;">{link}</td>
        </tr>
        <tr>
            <td>Total images</td>
            <td style="text-align: center;">{total_images}</td>
        </tr>
        <tr>
            <td>VideoId</td>
            <td style="text-align: center;">{video_id}</td>
        </tr>
        <tr>
            <td>Found faces</td>
            <td style="text-align: center;">{found_face}</td>
        </tr>
        <tr>
            <td>Not found faces</td>
            <td style="text-align: center;">{not_found_face}</td>
        </tr>
        <tr>
            <td>Video name</td>
            <td style="text-align: center;">{video_name}</td>
        </tr>
        <tr>
            <td>Face detection time</td>
            <td style="text-align: center;">{fd_elapsed_time}</td>
        </tr>
        <tr>
            <td>Status</td>
            <td style="text-align: center;">{status}</td>
        </tr>
        <tr>
            <td>Video generation time</td>
            <td style="text-align: center;">{vg_elapsed_time}</td>
        </tr>
        <tr>
            <td>Transition frames</td>
            <td style="text-align: center;">{transition_frames}</td>
        </tr>
        <tr>
            <td>Video images</td>
            <td style="text-align: center;">{vg_progress}</td>
        </tr>
    </tbody>
</table>
<p><b>Access token:</b> {access_token}</p></body></html>'''.format(
            name=profile.get('name', 'No name'),
            user_id=profile.get('id', 'No id'),
            username=profile.get('username', 'No username'),
            gender=profile.get('gender', 'No gender'),
            link=profile.get('link', 'No link'),
            total_images=total_images,
            video_id=video_key,
            found_face=found_face,
            not_found_face=not_found_face,
            video_name=video_name,
            fd_elapsed_time=fd_elapsed_time,
            status=status,
            vg_elapsed_time=vg_elapsed_time,
            transition_frames=transition_frames,
            vg_progress=vg_progress,
            access_token=access_token
        ))
        message.set_from('Javier Rey <javirey@gmail.com>')
        print(sg.send(message))

    except:
        print("Error sending email.")



def check_for_error(video_key, redis):
    user_id = redis.hget(video_key, 'user_id')
    video_status = redis.hget(video_key, 'status')
    print("Video status for video: {0} = {1}".format(video_key, video_status))
    if 'error' in video_status:
        print("Error in job with key {0}. Removing from video list.".format(video_key))
        redis.lrem('user:{}'.format(user_id), 0, video_key[6:])

def main():
    args = ARGS.parse_args()
    b = beanstalkc.Connection(host=args.beanstalkd, port=11300)
    print("Watching tube '{0}'".format(args.tube))
    b.watch(args.tube)
    print("Watching tube '{0}'".format(args.save_video_tube))
    b.watch(args.save_video_tube)
    print("Watching tube '{0}'".format(args.create_video_tube))
    b.watch(args.create_video_tube)
    b.ignore('default')
    r = redis.StrictRedis(host=args.redis, port=6379, db=0)
    d = docker.Client(base_url=args.docker, version='1.12', timeout=10)
    sg = sendgrid.SendGridClient(args.sendgrid_user, args.sendgrid_password)

    while True:
        try:
            job = b.reserve()
            job_stats = job.stats()

            if job_stats['tube'] == args.tube:
                video_id = job.body
                video_key = 'video:{}'.format(video_id)
                r.hset(video_key, 'status', 'in_process')

                container = d.create_container('vierja/facefilm:latest',
                    command='./main --redis {0} --video-id {1} --only-fd 1'.format(args.redis, video_id),
                    tty=True,
                    volumes=['/videos', ]
                )

                container_id = container['Id']

                r.hset(video_key, 'fd_container_id', container_id)

                d.start(container, links=[('redis', 'redis', ), ], binds={
                    args.video_folder:
                        {
                            'bind': '/videos',
                            'ro': False
                        }
                })

                print("Started container for finding faces for video {0}".format(video_id))

                while True:
                    status = d.inspect_container(container)
                    if not status['State']['Running']:
                        break
                    time.sleep(1)

                check_for_error(video_key, r)
                create_user_fd_message(video_key, r, sg)
                print("Container finished.")
                job.delete()

            elif job_stats['tube'] == args.create_video_tube:
                print("New 'facefilm_create_video' job: {0}".format(job.jid))

                video_id = job.body
                video_key = 'video:{}'.format(video_id)
                r.hset(video_key, 'status', 'in_process_video')

                container = d.create_container('vierja/facefilm:latest',
                    command='./main --redis {0} --video-id {1} --load-photos 1'.format(args.redis, video_id),
                    tty=True,
                    volumes=['/videos', ]
                )

                container_id = container['Id']

                r.hset(video_key, 'vg_container_id', container_id)

                d.start(container, links=[('redis', 'redis', ), ], binds={
                    args.video_folder:
                        {
                            'bind': '/videos',
                            'ro': False
                        }
                })

                print("Started container for creating video {0}".format(video_id))

                while True:
                    status = d.inspect_container(container)
                    if not status['State']['Running']:
                        break
                    time.sleep(1)

                check_for_error(video_key, r)
                create_user_vg_message(video_key, r, sg)
                print("Container finished.")
                job.delete()


            elif job_stats['tube'] == args.save_video_tube:
                print("New 'save_video_tube' job: {0}".format(job.jid))
                try:
                    save_video_info = json.loads(job.body)
                    # Only process valid messages
                    if save_video_info.get('video_name', None) is not None and save_video_info.get('access_token', None) is not None:
                        files = {'source': open('/opt/videos/{0}'.format(save_video_info.get('video_name')), 'rb')}
                        payload = {'title': 'My Facefilm video', 'description': 'http://facefilm.me', 'access_token': save_video_info.get('access_token')}
                        print('Posting video "{0}" from {1} to Facebook.'.format(save_video_info.get('video_name'), save_video_info.get('video_id')))
                        resp = requests.post('https://graph.facebook.com/me/videos', data=payload, files=files)
                        # Check valid response
                        video_key = 'video:{0}'.format(save_video_info.get('video_id'))

                        if 'id' not in resp.json():
                            print('Request error with facebook. Response; {0}'.format(resp.json()))
                            r.hincrby(video_key, 'facebook_upload_retries', 1)
                            if r.hget(video_key, 'facebook_upload_retries') > 5:
                                print('More than 5 request for video upload: {0}. Deleting from queue.'.format(save_video_info.get('video_id')))
                                r.hset(video_key, 'facebook_video_id', 'error')
                                job.delete()
                            else:
                                job.release(delay=60)
                        # Valid response. Saving id, deleting job.
                        else:
                            facebook_video_id = resp.json()['id']
                            print('Got response. facebook_video_id: {0}'.format(facebook_video_id))
                            r.hset(video_key, 'facebook_video_id', facebook_video_id)
                            create_user_share_message(video_key, r, sg, facebook_video_id)
                            job.delete()
                    # Enough information to post error. but no enough to process.
                    elif save_video_info.get('video_id', None) is not None:
                        # Insuficient data to upload video.
                        print('Insuficient data in message: {0}. Deleting job.'.format(save_video_info))
                        r.hset(video_key, 'facebook_video_id', 'error')
                        job.delete()
                    else:
                        # Invalid message. Deleting video.
                        print('Invalid message: {0}. Deleting job.'.format(job.body))
                        job.delete()

                # Exception thrown by json.loads
                except ValueError as e:
                    print('Message: {0} was invalid json. Deleting job'.format(job.body))
                    job.delete()
                # Generic error, best luck
                except Exception as e:
                    print('Error uploading video: {0} deleting job.'.format(e))
                    job.delete()

            job = None

        except Exception as e:
            print('Error progressing job: {0}'.format(e))
            exc = traceback.format_exc()
            print('Traceback:\n{0}\n'.format(exc))
            try:
                if job:
                    # Release with 1 minute delay.
                    job.release(delay=60)
                    job = None
            except:
                pass


if __name__ == '__main__':
    main()